import React from 'react';
import { AppRegistry } from 'react-native';
import { Provider } from 'react-redux';
import App from './app';
import createStore from './createStore';

export default class empowerApp extends React.Component {

    render() {
        const store = createStore();
        return (
            <Provider store={store}>
              <App />
            </Provider>
        );
    }
}

AppRegistry.registerComponent('empowerapp', () => empowerApp);
