class Constant {
    static BASE_URL = "http://www.empowerednurses.org/";
    static BASE_URL_2 = "http://yournurseattorney.com/";
    static BASE_URL_3= "https://marketplace.infusionsoft.com/";
    static login="https://bu162.infusionsoft.com/app/form/process/32482ae20d3415ff8951fa80f2765d0d";
    //static BASE_URL = "http://192.168.1.7:3000/";
    static URL_login = Constant.BASE_URL +'form/index.php';

    static URL_home = Constant.BASE_URL;
    static URL_privacy =Constant.BASE_URL +'privacy-policy';
    static URL_aboutus = Constant.BASE_URL + 'who-we-are';
    static URL_blog = Constant.BASE_URL + 'category/blog';
    static URL_contact = Constant.BASE_URL + 'contact';

    static URL_licence = Constant.BASE_URL_2 + 'how-to-protect-your-license';
    static URL_news = Constant.BASE_URL + 'category/news';
    static URL_nurse_act = Constant.BASE_URL + 'nurse-practice-acts';

    static URL_shop = Constant.BASE_URL_3+'listing/biz-x-tech-mobile-friendly-ecommerce-template-by-inpressto';
}

var WebServices = {
  callWebService: function(url, body) {
    var formBody = [];
    for (var property in body) {
      var encodedKey = encodeURIComponent(property);
      var encodedValue = encodeURIComponent(body[property]);
      formBody.push(encodedKey + "=" + encodedValue);
    }
    formBody = formBody.join("&");
    console.log(formBody);
      return fetch(url, {
              method: 'POST',
              headers: {
                  //'Accept': 'application/json',
                  'Accept':'application/html; charset=utf-8',
                  'Content-Type':'application/x-www-form-urlencoded;charset=UTF-8'
                  //'CallToken': body.CallToken ? body.CallToken : '',
              },
              body: formBody
          })
          .then(function(response) {
              console.log(response)
          })
        /*  .then(response => response.text()) // Convert to text instead of res.json()
          .then((text) => {
              return text;
          })
          .then(response => JSON.parse(response)) // Parse the text.
          .then((jsonRes) => {
            console.log(jsonRes)
              //return jsonRes; //main output
          });*/
  },

    callWebService_GET: function(url, body) {
        return fetch(url, { // Use your url here
                method: 'GET',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'CallToken': body.CallToken ? body.CallToken : '',
                },
            })
            .then(response => response.text()) // Convert to text instead of res.json()
            .then((text) => {

                return text;
            })
            .then(response => JSON.parse(response)) // Parse the text.
            .then((jsonRes) => {

                // if ((!jsonRes) || (jsonRes.ResponseCode != '200')) {
                //     onError(jsonRes);
                // } else {
                //     onRequestComplete(jsonRes);
                // }

                return jsonRes;
            });
    }
}
module.exports = {
    Constant,
    WebServices
}
