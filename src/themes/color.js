const app = {
    primary : '#37a9c5',
    secondary : '#ddd',
    background: '#FFFFFF',
    cardBackground: '#FFFFFF',
    listItemBackground: '#FFFFFF',
};

const brand = {
    brand: {
        primary: '#FFFFFF',
        secondary: '#17233D',
    },
};
const text = {
    textPrimary: 'black',
    textSecondary: '#777777',
    headingPrimary: brand.brand.primary,
    headingSecondary: brand.brand.primary,
};

const borders = {
    border: '#D0D1D5',
};

const tabbar = {
    tabbar: {
        background: '#ffffff',
        iconDefault: '#BABDC2',
        iconSelected: brand.brand.primary,
    },
};


export default {
    ...app,
    ...brand,
    ...text,
    ...borders,
    ...tabbar,
};
