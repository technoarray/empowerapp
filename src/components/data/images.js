import React, { Component } from 'react';
import {StatusBar,Platform,StyleSheet,Text,View,Image,TouchableOpacity} from 'react-native';
import {base64images} from './base64images';
import {base64images2} from './base64images2';
import {base64images3} from './base64images3';
import {base64images4} from './base64images4';
import {base64images5} from './base64images5';
import {base64images6} from './base64images6';
import {base64images7} from './base64images7';
import {base64images8} from './base64images8';
import {base64images9} from './base64images9';
import {base64images10} from './base64images10';
import {base64images11} from './base64images11';

if(Platform.OS === 'ios') {
     var back_card2 = require( '../../themes/Images/cards/ios/2.png')
     var back_card3 = require( '../../themes/Images/cards/ios/3.png')
     var back_card4 = require( '../../themes/Images/cards/ios/4.png')
     var back_card5 = require( '../../themes/Images/cards/ios/5.png')
     var back_card6 = require( '../../themes/Images/cards/ios/6.png')
     var back_card7 = require( '../../themes/Images/cards/ios/7.png')
     var back_card8 = require( '../../themes/Images/cards/ios/8.png')
     var back_card9 = require( '../../themes/Images/cards/ios/9.png')
     var back_card10 = require( '../../themes/Images/cards/ios/10.png')
     var back_card11 = require( '../../themes/Images/cards/ios/11.png')
     var back_card12 = require( '../../themes/Images/cards/ios/12.png')
     var back_card13 = require( '../../themes/Images/cards/ios/13.png')
     var back_card14 = require( '../../themes/Images/cards/ios/14.png')
     var back_card15 = require( '../../themes/Images/cards/ios/15.png')
     var back_card16 = require( '../../themes/Images/cards/ios/16.png')
     var back_card17 = require( '../../themes/Images/cards/ios/17.png')
     var back_card18 = require( '../../themes/Images/cards/ios/18.png')
     var back_card19 = require( '../../themes/Images/cards/ios/19.png')
     var back_card20 = require( '../../themes/Images/cards/ios/20.png')
     var back_card21 = require( '../../themes/Images/cards/ios/21.png')
     var back_card22 = require( '../../themes/Images/cards/ios/22.png')
     var back_card23 = require( '../../themes/Images/cards/ios/23.png')
     var back_card24 = require( '../../themes/Images/cards/ios/24.png')
     var back_card25 = require( '../../themes/Images/cards/ios/25.png')
     var back_card26 = require( '../../themes/Images/cards/ios/26.png')
     var back_card27 = require( '../../themes/Images/cards/ios/27.png')
     var back_card28 = require( '../../themes/Images/cards/ios/28.png')
     var back_card29 = require( '../../themes/Images/cards/ios/29.png')
     var back_card30 = require( '../../themes/Images/cards/ios/30.png')
     var back_card31 = require( '../../themes/Images/cards/ios/31.png')
     var back_card32 = require( '../../themes/Images/cards/ios/32.png')
     var back_card33 = require( '../../themes/Images/cards/ios/33.png')
     var back_card34 = require( '../../themes/Images/cards/ios/34.png')
     var back_card35 = require( '../../themes/Images/cards/ios/35.png')
     var back_card36 = require( '../../themes/Images/cards/ios/36.png')
     var back_card37 = require( '../../themes/Images/cards/ios/37.png')
     var back_card38 = require( '../../themes/Images/cards/ios/38.png')
     var back_card39 = require( '../../themes/Images/cards/ios/39.png')
     var back_card40 = require( '../../themes/Images/cards/ios/40.png')
     var back_card41 = require( '../../themes/Images/cards/ios/41.png')
     var back_card42 = require( '../../themes/Images/cards/ios/42.png')
     var back_card43 = require( '../../themes/Images/cards/ios/43.png')
     var back_card44 = require( '../../themes/Images/cards/ios/44.png')
     var back_card45 = require( '../../themes/Images/cards/ios/45.png')
     var back_card46 = require( '../../themes/Images/cards/ios/46.png')
     var back_card47 = require( '../../themes/Images/cards/ios/47.png')
     var back_card48 = require( '../../themes/Images/cards/ios/48.png')
     var back_card49 = require( '../../themes/Images/cards/ios/49.png')
     var back_card50 = require( '../../themes/Images/cards/ios/50.png')
     var back_card51 = require( '../../themes/Images/cards/ios/51.png')
     var back_card52 = require( '../../themes/Images/cards/ios/52.png')
     var back_card53 = require( '../../themes/Images/cards/ios/53.png')
     var back_card54 = require( '../../themes/Images/cards/ios/54.png')
     var back_card55 = require( '../../themes/Images/cards/ios/55.png')
} else {
    var back_card2 = require( '../../themes/Images/cards/android/2.png')
    var back_card3 = require( '../../themes/Images/cards/android/3.png')
    var back_card4 = require( '../../themes/Images/cards/android/4.png')
    var back_card5 = require( '../../themes/Images/cards/android/5.png')
    var back_card6 = require( '../../themes/Images/cards/android/6.png')
    var back_card7 = require( '../../themes/Images/cards/android/7.png')
    var back_card8 = require( '../../themes/Images/cards/android/8.png')
    var back_card9 = require( '../../themes/Images/cards/android/9.png')
    var back_card10 = require( '../../themes/Images/cards/android/10.png')
    var back_card11 = require( '../../themes/Images/cards/android/11.png')
    var back_card12 = require( '../../themes/Images/cards/android/12.png')
    var back_card13 = require( '../../themes/Images/cards/android/13.png')
    var back_card14 = require( '../../themes/Images/cards/android/14.png')
    var back_card15 = require( '../../themes/Images/cards/android/15.png')
    var back_card16 = require( '../../themes/Images/cards/android/16.png')
    var back_card17 = require( '../../themes/Images/cards/android/17.png')
    var back_card18 = require( '../../themes/Images/cards/android/18.png')
    var back_card19 = require( '../../themes/Images/cards/android/19.png')
    var back_card20 = require( '../../themes/Images/cards/android/20.png')
    var back_card21 = require( '../../themes/Images/cards/android/21.png')
    var back_card22 = require( '../../themes/Images/cards/android/22.png')
    var back_card23 = require( '../../themes/Images/cards/android/23.png')
    var back_card24 = require( '../../themes/Images/cards/android/24.png')
    var back_card25 = require( '../../themes/Images/cards/android/25.png')
    var back_card26 = require( '../../themes/Images/cards/android/26.png')
    var back_card27 = require( '../../themes/Images/cards/android/27.png')
    var back_card28 = require( '../../themes/Images/cards/android/28.png')
    var back_card29 = require( '../../themes/Images/cards/android/29.png')
    var back_card30 = require( '../../themes/Images/cards/android/30.png')
    var back_card31 = require( '../../themes/Images/cards/android/31.png')
    var back_card32 = require( '../../themes/Images/cards/android/32.png')
    var back_card33 = require( '../../themes/Images/cards/android/33.png')
    var back_card34 = require( '../../themes/Images/cards/android/34.png')
    var back_card35 = require( '../../themes/Images/cards/android/35.png')
    var back_card36 = require( '../../themes/Images/cards/android/36.png')
    var back_card37 = require( '../../themes/Images/cards/android/37.png')
    var back_card38 = require( '../../themes/Images/cards/android/38.png')
    var back_card39 = require( '../../themes/Images/cards/android/39.png')
    var back_card40 = require( '../../themes/Images/cards/android/40.png')
    var back_card41 = require( '../../themes/Images/cards/android/41.png')
    var back_card42 = require( '../../themes/Images/cards/android/42.png')
    var back_card43 = require( '../../themes/Images/cards/android/43.png')
    var back_card44 = require( '../../themes/Images/cards/android/44.png')
    var back_card45 = require( '../../themes/Images/cards/android/45.png')
    var back_card46 = require( '../../themes/Images/cards/android/46.png')
    var back_card47 = require( '../../themes/Images/cards/android/47.png')
    var back_card48 = require( '../../themes/Images/cards/android/48.png')
    var back_card49 = require( '../../themes/Images/cards/android/49.png')
    var back_card50 = require( '../../themes/Images/cards/android/50.png')
    var back_card51 = require( '../../themes/Images/cards/android/51.png')
    var back_card52 = require( '../../themes/Images/cards/android/52.png')
    var back_card53 = require( '../../themes/Images/cards/android/53.png')
    var back_card54 = require( '../../themes/Images/cards/android/54.png')
    var back_card55 = require( '../../themes/Images/cards/android/55.png')
}

 export const jsonImages={
   0:{
    "url": back_card2,
    "base64": base64images[0].image2
  },
  1:{
    "url": back_card3,
    "base64": base64images[1].image3
  },
  2:{
    "url": back_card4,
    "base64": base64images[2].image4
  },
  3:{
    "url": back_card5,
    "base64": base64images[3].image5
  },
  4:{
    "url": back_card6,
    "base64": base64images[4].image6
  },
  5:{
    "url": back_card7,
    "base64": base64images2[0].image7
  },
  6:{
    "url": back_card8,
    "base64": base64images2[1].image8
  },
  7:{
    "url": back_card9,
    "base64": base64images2[2].image9
  },
  8:{
    "url": back_card10,
    "base64": base64images2[3].image10
  },
  9:{
    "url": back_card11,
    "base64": base64images2[4].image11
  },
  10:{
    "url": back_card12,
    "base64": base64images3[0].image12
  },
  11:{
    "url": back_card13,
    "base64": base64images3[1].image13
  },
  12:{
    "url": back_card14,
    "base64": base64images3[2].image14
  },
  13:{
    "url": back_card15,
    "base64": base64images3[3].image15
  },
  14:{
    "url": back_card16,
    "base64": base64images3[4].image16
  },
  15:{
    "url": back_card17,
    "base64": base64images4[0].image17
  },
  16:{
    "url": back_card18,
    "base64": base64images4[1].image18
  },
  17:{
    "url": back_card19,
    "base64": base64images4[2].image19
  },
  18:{
    "url": back_card20,
    "base64": base64images4[3].image20
  },
  19:{
    "url": back_card21,
    "base64": base64images4[4].image21
  },
  20:{
    "url": back_card22,
    "base64": base64images5[0].image22
  },
  21:{
    "url": back_card23,
    "base64": base64images5[1].image23
  },
  22:{
    "url": back_card24,
    "base64": base64images5[2].image24
  },
  23:{
    "url": back_card25,
    "base64": base64images5[3].image25
  },
  24:{
    "url": back_card26,
    "base64": base64images5[4].image26
  },
  25:{
    "url": back_card27,
    "base64": base64images6[0].image27
  },
  26:{
    "url": back_card28,
    "base64": base64images6[1].image28
  },
  27:{
    "url": back_card29,
    "base64": base64images6[2].image29
  },
  28:{
    "url": back_card30,
    "base64": base64images6[3].image30
  },
  29:{
    "url": back_card31,
    "base64": base64images6[4].image31
  },
  30:{
    "url": back_card32,
    "base64": base64images7[0].image32
  },
  31:{
    "url": back_card33,
    "base64": base64images7[1].image33
  },
  32:{
    "url": back_card34,
    "base64": base64images7[2].image34
  },
  33:{
    "url": back_card35,
    "base64": base64images7[3].image35
  },
  34:{
    "url": back_card36,
    "base64": base64images7[4].image36
  },
  35:{
    "url": back_card37,
    "base64": base64images8[0].image37
  },
  36:{
    "url": back_card38,
    "base64": base64images8[1].image38
  },
  37:{
    "url": back_card39,
    "base64": base64images8[2].image39
  },
  38:{
    "url": back_card40,
    "base64": base64images8[3].image40
  },
  39:{
    "url": back_card41,
    "base64": base64images8[4].image41
  },
  40:{
    "url": back_card42,
    "base64": base64images9[0].image42
  },
  41:{
    "url": back_card43,
    "base64": base64images9[1].image43
  },
  42:{
    "url": back_card44,
    "base64": base64images9[2].image44
  },
  43:{
    "url": back_card45,
    "base64": base64images9[3].image45
  },
  44:{
    "url": back_card46,
    "base64": base64images9[4].image46
  },
  45:{
    "url": back_card47,
    "base64": base64images10[0].image47
  },
  46:{
    "url": back_card48,
    "base64": base64images10[1].image48
  },
  47:{
    "url": back_card49,
    "base64": base64images10[2].image49
  },
  48:{
    "url": back_card50,
    "base64": base64images10[3].image50
  },
  49:{
    "url": back_card51,
    "base64": base64images10[4].image51
  },
  50:{
    "url": back_card52,
    "base64": base64images11[0].image52
  },
  51:{
    "url": back_card53,
    "base64": base64images11[1].image53
  },
  52:{
    "url": back_card54,
    "base64": base64images11[2].image54
  },
  53:{
    "url": back_card55,
    "base64": base64images11[3].image55
  }

}
