import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    Image,
    TouchableHighlight,
    ScrollView,
    Dimensions,
    TextInput,
    TouchableOpacity,
    Alert,
    Keyboard,
    NetInfo,
    Platform,
    AsyncStorage,
    StatusBar
} from 'react-native';
import * as stateActions from '../../stateActions';
import { connect } from 'react-redux';
import { NavigationActions } from 'react-navigation'
import { AppStyles, AppSizes, AppColors } from '../../themes/'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import Spinner from 'react-native-loading-spinner-overlay';
import LinearGradient from 'react-native-linear-gradient';
import Spacer from '../common/Spacer'
import * as commonFunctions from '../../utils/CommonFunctions'
var Constant = require('../../api/ApiRules').Constant;
var WebServices = require('../../api/ApiRules').WebServices;

const logo = require('../../themes/Images/logo.png')

let _that
export default class LoginScreen extends Component {
    constructor(props) {
        super(props)
        this.state = {
          name: '',
          email: '',
          isVisible: false,
        },
        _that = this;
    }

    componentWillMount() {
      AsyncStorage.getItem('loggedIn').then((value) => {
            var loggedIn = JSON.parse(value);
            if(loggedIn){
              const resetAction = NavigationActions.navigate({ routeName: 'StartScreen'})
              _that.props.navigation.dispatch(resetAction);
            }
        })
    }

  userLogin() {
    _that.validationAndApiParameter()
  }

  validationAndApiParameter() {
        const { name, email } = this.state

        if ((name.indexOf(' ') >= 0 || name.length <= 0)) {
            Alert.alert('','Please enter Name!');
        } else if ((email.indexOf(' ') >= 0 || email.length <= 0)) {
            Alert.alert('','Please enter Email address!');
        }else if (!commonFunctions.validateEmail(email)) {
            Alert.alert('','Please enter valid Email address!');
        }
        else{
            var data = {
              inf_form_xid: "32482ae20d3415ff8951fa80f2765d0d",
              inf_form_name: "EmpowerApp Form",
              infusionsoft_version: "1.70.0.69529",
              inf_field_FirstName: name,
              inf_field_Email: email
            };
            console.log(data);
            _that.setState({
                      isVisible: true
                  });
            this.postToApiCalling('POST', 'login', Constant.URL_login, data);
        }
  }

  postToApiCalling(method, apiKey, apiUrl, data) {

     new Promise(function(resolve, reject) {
          if (method == 'POST') {
              resolve(WebServices.callWebService(apiUrl, data));
          } else {
              resolve(WebServices.callWebService_GET(apiUrl, data));
          }
      }).then((jsonRes) => {
          _that.setState({ isVisible: false })
          console.log(jsonRes)
      }).catch((error) => {
        console.log(error)
          console.log("ERROR" + error);
          _that.setState({ isVisible: false })

          setTimeout(()=>{
              Alert.alert("Server issue");
          },200);
      });
  }

    static navigationOptions = {
        header: null,
    }


    render() {
        return (
            <View style={{flex:1, justifyContent: 'center',alignItems: 'center',backgroundColor: '#396c99'}}>
               <StatusBar hidden={true} />
                <LinearGradient colors={['#3badcb', '#3996ba', '#3a8cb2', '#38709d']}>
                <View style={{margin:30}}>
                <KeyboardAwareScrollView
                    innerRef={() => {return [this.refs.username, this.refs.email]}} >
                    <Spacer size={15} />
                    <View style={{width:AppSizes.screen.width-120,height:100,justifyContent: 'center', margin: 10}}>
                            <Image source={logo} style={{flexGrow:1,height:null,width:null,alignItems: 'center',justifyContent:'center',resizeMode:'contain',}} />
                    </View>
                    <View style={{ marginTop:10}}>
                       <View>
                             {/*<View style={{flexDirection: 'row'}}>
                                <Text style={styles.texttitle}>EMPOWERED N</Text>
                                  <View style={{borderBottomWidth:2,borderBottomColor:'#ffffff'}}>
                                    <Text style={[styles.texttitle,{paddingBottom:5}]}>URSE</Text>
                                  </View>
                                <Text style={styles.texttitle}>S CHALLENGE</Text>
                            </View>

                        <Spacer size={15} />*/}
                        <Text style={styles.textsubtitle}>Please share your email with us to join our community and access the cards</Text>
                       </View>
                        <View style={{marginTop:5}}>
                            <TextInput
                                    autoCapitalize={'none'}
                                    autoCorrect={false}
                                    underlineColorAndroid="transparent"
                                    returnKeyType={ "next"}
                                    selectionColor={"#FFFFFF"}
                                    autoFocus={ false}
                                    placeholder="Name"
                                    placeholderTextColor="#fff"
                                    style={styles.textInput}
                                    ref="name"
                                    keyboardType={ 'email-address'}
                                    onChangeText={name=> this.setState({name})}

                            />
                        </View>
                    </View>
                    <View style={{ marginTop:10}}>
                        <View style={{}}>
                            <TextInput
                                   autoCapitalize={'none'}
                                    autoCorrect={false}
                                    underlineColorAndroid="transparent"
                                    returnKeyType={ "done"}
                                    selectionColor={"#FFFFFF"}
                                    autoFocus={ false}
                                    placeholder="Email"
                                    placeholderTextColor="#fff"
                                    style={styles.textInput}
                                    ref="email"
                                    keyboardType={ 'email-address'}
                                    onChangeText={email=> this.setState({email})}

                            />
                        </View>
                    </View>

                    <View style={{ alignItems:'center', justifyContent: 'center', marginBottom:10}}>
                        <View style={{ width: '100%', justifyContent:'space-around'}}>
                            <Spacer size={15} />
                            <TouchableOpacity activeOpacity={.6} onPress={this.userLogin}>
                                <View style={{borderWidth: 1, backgroundColor: 'white', width: '100%', height: 42, borderRadius: 20, alignItems:'center', justifyContent: 'center'}}>
                                    <Text style={styles.textbtn}>JOIN OUR COMMUNITY</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                        <Spacer size={12} />
                        <View style={{flex:.2, flexDirection: 'row', justifyContent: 'center', alignItems: 'center'}}>
                           <Text style={styles.textbottom}>Join our growing community of Empowered Nurses today and receive tips and strategies on license protection and empowerment.</Text>
                        </View>
                    </View>
                </KeyboardAwareScrollView>
                <Spinner visible={this.state.isVisible}  />
                </View>
                </LinearGradient>

            </View>
        );
    }
}

const styles = {
    text: {
        color: 'white',
        fontSize: 17,
        marginLeft: 10,
        backgroundColor:'transparent'
    },
    textInput: {
        height: 50,
        margin: 10,
        paddingLeft: 10,
        fontSize: 16,
        backgroundColor: 'transparent',
        borderWidth: 1,
        borderColor: '#fff',
        borderRadius: 20,
        color: 'white'
      },
    texttitle : {
      color:'#fff',
      fontSize:18,
      fontWeight:'bold',
      textAlign:'center',
    },
    textsubtitle: {
      color:'#fff',
      fontSize:14,
      textAlign:'center',
      lineHeight:18
    },
    textbtn:{
      color:'#5e85a8',
      fontSize:16,
      textAlign:'center',
    },
    textbottom:{
      color:'#fff',
      fontSize:11,
      textAlign:'center',
      letterSpacing:1,
      lineHeight:18
    }

}
