import {
    AppRegistry,
    StyleSheet,
    Dimensions,
    Button,
    Image,
    View,
    Text,
    ScrollView,
    TouchableOpacity,
    TouchableHighlight
} from 'react-native';
import { AppStyles, AppSizes, AppColors } from '../../themes/'
import React from 'react';

import { DrawerNavigator } from 'react-navigation';

import CardScreen from './CardScreen';
import HowToUseScreen from './HowToUseScreen';
import ShareExpScreen from './ShareExpScreen';
import AboutArtistScreen from './AboutArtistScreen';
import AboutCreatorScreen from './AboutCreatorScreen';
import ShopScreen from './ShopScreen';
import ContactScreen from './ContactScreen';
import LicenceScreen from './LicenceScreen';
import PrivacyScreen from './PrivacyScreen';
import SideMenu from './SideMenu';


export default DrawerNavigator({
    CardScreen: { screen: CardScreen},  //1 - cardNavOptions
    HowToUseScreen: { screen: HowToUseScreen},  //2 - aboutusNavOptions
    ShareExpScreen: { screen: ShareExpScreen}, //3 - newsNavOptions
    AboutArtistScreen: { screen: AboutArtistScreen}, //4 - shopNavOptions
    AboutCreatorScreen: { screen: AboutCreatorScreen}, //5 - AboutCreatorScreen
    PrivacyScreen: { screen: PrivacyScreen}, //6 - shopNavOptions
}, {
  contentComponent: SideMenu,
  drawerWidth: 300
});
