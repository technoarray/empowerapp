import React, { Component } from 'react';
import {StyleSheet,Text,View,Image,TouchableOpacity,WebView,Platform, ActivityIndicator} from 'react-native';
import { AppStyles, AppSizes, AppColors } from '../../themes/'
import { NavigationActions } from 'react-navigation'

var Constant = require('../../api/ApiRules').Constant;

/* Images */
var logo = require( '../../themes/Images/logo.png')
var menu_icon = require( '../../themes/Images/menu_icon_144.png')
var back_icon = require( '../../themes/Images/back_120.png')

const Constants = {
    javascript: {
        injection: `
            var div = document.getElementById("header-title-logo");
            div.style.display = "none";
        `
    }
}

export default class BlogScreen extends Component {
  constructor (props) {
    super(props)
    this.state = {
    isVisible: false,
  },
  _that = this;
}

  menu_click() {
      _that.props.navigation.openDrawer();
  }
  go_back () {
      _that.props.navigation.navigate('CardScreen');
  }

  ActivityIndicatorLoadingView() {

      return (

        <ActivityIndicator
          color='#fff'
          size='large'
          style={styles.ActivityIndicatorStyle}
        />
      );
    }

  render() {
    return (
      <View style={styles.container}>

      <View style={{ flex:.07,flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', marginTop: 20,marginBottom: 20}}>
          <View style={{ marginTop:0, paddingLeft:10}}>
              <TouchableOpacity onPress={this.menu_click}>
                  <Image source={menu_icon} style={{resizeMode: 'contain', width: 40, height: 40, }} />
              </TouchableOpacity>
          </View>
          <View style={{ marginTop:0}}>
              <Image source={logo} style={{resizeMode: 'contain', height: 200, width: 200}} />
          </View>
          <View style={{ marginTop:0}}>

          </View>
      </View>

      <View style={{flex:.95, width: AppSizes.screen.width,}}>
      <WebView
      style={styles.WebViewStyle}
      source={{uri: Constant.URL_blog}}
      injectedJavaScript={Constants.javascript.injection}
      mixedContentMode={'compatibility'}
      javaScriptEnabled={true}
      renderLoading={this.ActivityIndicatorLoadingView}
      startInLoadingState={true}
      />
      </View>
      <View style={{position:'absolute',bottom:20,right:20,alignSelf:'flex-end'}}>
        <TouchableOpacity onPress={this.go_back}>
            <Image source={back_icon} style={{resizeMode: 'contain', width: 60, height: 60, }} />
        </TouchableOpacity>
        </View>
    </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 0,
    backgroundColor: '#3bafca',
  },
  WebViewStyle:
  {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop:  0
  },

  ActivityIndicatorStyle:{
     position: 'absolute',
     left: 0,
     right: 0,
     top: 0,
     bottom: 0,
     alignItems: 'center',
     justifyContent: 'center'

  }

});
