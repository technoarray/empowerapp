import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    Image,
    TouchableHighlight,
    ScrollView,
    Dimensions,
    TextInput,
    TouchableOpacity,
    Alert,
    Keyboard,
    NetInfo,
    Platform,
    WebView,
    ActivityIndicator,
    AsyncStorage,
    StatusBar
} from 'react-native';
import * as stateActions from '../../stateActions';
import { connect } from 'react-redux';
import { NavigationActions } from 'react-navigation'
import { AppStyles, AppSizes, AppColors } from '../../themes/'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import Spinner from 'react-native-loading-spinner-overlay';
import LinearGradient from 'react-native-linear-gradient';
import Spacer from '../common/Spacer'
import * as commonFunctions from '../../utils/CommonFunctions'
var Constant = require('../../api/ApiRules').Constant;
var WebServices = require('../../api/ApiRules').WebServices;

/* Images */
var logo = require( '../../themes/Images/logo.png')
var menu_icon = require( '../../themes/Images/menu_icon_144.png')
var back_icon = require( '../../themes/Images/back_120.png')

const MyStatusBar = ({backgroundColor, ...props}) => (
  <View style={[styles.statusBar, { backgroundColor }]}>
    <StatusBar translucent backgroundColor={backgroundColor} {...props} />
  </View>
);

export default class LoginScreen extends Component {
  constructor (props) {
    super(props)
    this.state = {
     //url: 'https://www.google.com',
    url:Constant.URL_login,
    isVisible: false,

  },
  _that = this;
}

componentDidMount() {
  AsyncStorage.getItem('loggedIn').then((value) => {
        var loggedIn = JSON.parse(value);
        if(loggedIn){
          const resetAction = NavigationActions.navigate({ routeName: 'StartScreen'})
          _that.props.navigation.dispatch(resetAction);
        }
    })
}


  ActivityIndicatorLoadingView() {

      return (
        <ActivityIndicator
          color='#fff'
          size='large'
          style={styles.ActivityIndicatorStyle}
        />
      );
    }

    handleNavigationStateChange = navState => {
      //console.log(navState.url);
      if(navState.url.indexOf('process') !== -1)
      {
        _that.setState({isVisible: true,});
      }
      if(navState.url.indexOf('contactId') !== -1)
      {
        queryArr = navState.url.replace('?','').split('&'),
        queryParams = [];

        for (var q = 0, qArrLength = queryArr.length; q < qArrLength; q++) {
            var qArr = queryArr[q].split('=');
            queryParams[qArr[0]] = qArr[1];
        }
        //console.log(queryParams);
        name=queryParams['inf_field_FirstName'];
        email=queryParams['inf_field_Email']
        _that.setState({isVisible: false,});
        //console.log(name+ " "+email)
        AsyncStorage.setItem("loggedIn", JSON.stringify(true)).done();
        AsyncStorage.setItem('name', JSON.stringify(name));
        AsyncStorage.setItem('email', JSON.stringify(email));
        setTimeout(()=>{
          _that.props.navigation.navigate('afterLoginScreen');
        },100);
      }

    };

  render() {
    const injectedJs = `
      window.postMessage(window.location.href);
    `;
    return (
      <View style={styles.container}>
      <MyStatusBar barStyle="light-content"  backgroundColor="#3baeca"/>

          <WebView
          style={styles.WebViewStyle}
          source={{ uri: this.state.url }}
          bounces={true}
          injectedJavaScript={injectedJs}
          javaScriptEnabledAndroid={true}
          javaScriptEnabled={true}
          onNavigationStateChange={this.handleNavigationStateChange}
          renderLoading={this.ActivityIndicatorLoadingView}
          startInLoadingState={true}
          scalesPageToFit
           mixedContentMode={'compatibility'}
          />
          <Spinner visible={this.state.isVisible}  />
          </View>
    );
  }
}

const styles = {
  container:{
    flex:1,
    backgroundColor: '#2a699f'
  },
  statusBar: {
    height: AppSizes.statusBarHeight,
  },
    WebViewStyle:
    {
      flex:1,

    },

    ActivityIndicatorStyle:{
       position: 'absolute',
       left: 0,
       right: 0,
       top: 0,
       bottom: 0,
       alignItems: 'center',
       justifyContent: 'center'

    }

}
