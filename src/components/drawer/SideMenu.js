import PropTypes from 'prop-types';
import React, {Component} from 'react';
import {NavigationActions} from 'react-navigation';
import {AsyncStorage,StyleSheet,ScrollView, Text, View,TouchableOpacity, Image,Linking,Alert} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';

var Constant = require('../../api/ApiRules').Constant;

var close_icon = require( '../../themes/Images/cross_120.png')
var divider1 = require( '../../themes/Images/divider-1.jpg')


class SideMenu extends Component {
  constructor(props) {
      super(props)
      this.state = {
        name: '',
      };
      _that = this;
  }
  componentDidMount() {
      AsyncStorage.getItem('loggedIn').then((value) => {
          var loggedIn = JSON.parse(value);
          if (loggedIn) {
              AsyncStorage.getItem('name').then((name) => {
                  _that.setState({name: name  });
              })
          }
      })
}

  navigateToScreen = (route) => () => {
    const navigateAction = NavigationActions.navigate({
      routeName: route,
    });
    _that.props.navigation.dispatch(navigateAction);
  }

  menu_close() {
    _that.props.navigation.closeDrawer();
  }

  our_website() {
    Alert.alert(
        '',
        'You will be redirected to your default browser, to visit our Website',
        [
          {text: 'Cancel', onPress: () => console.log('Cancel Button Pressed'), style: 'cancel'},
          {text: 'OK', onPress: () => Linking.openURL(Constant.URL_home)},

        ]

      )
 }

  render () {
    return (
      <View style={styles.container}>
        <ScrollView>
        <LinearGradient colors={['#3baeca', '#3baeca', '#3983ab', '#386b98']}>
          <View style={{ margin:25,alignSelf: 'flex-end',}}>
            <TouchableOpacity onPress={this.menu_close}>
                <Image source={close_icon} style={{resizeMode: 'contain', width: 50, height: 50, }} />
            </TouchableOpacity>
            </View>

            <View style={{ margin:10,}}>
              {/*<Text style={[styles.navtitle,{textTransform:'uppercase',}]}>Name {this.state.name}</Text>*/}
            </View>

            <View style={styles.navSectionStyle}>
              {/*<View style={[styles.navBorderStyle,{borderBottomColor:'#76cadd',borderTopWidth:2, borderTopColor:'#76cadd'}]} >*/}

              <View style={[styles.navBorderStyle,{borderBottomColor:'#ffffff',}]} >
                <Text style={[styles.navItemStyle,{color:'#ffffff'}]} onPress={this.navigateToScreen('HowToUseScreen')}>
                  How to use the cards
                  </Text>
                </View>
                <View style={[styles.navBorderStyle,{borderBottomColor:'#ffffff',}]} >
                  <Text style={[styles.navItemStyle,{color:'#ffffff'}]} onPress={this.navigateToScreen('AboutCreatorScreen')}>
                    About the creator
                  </Text>
                </View>
                <View style={[styles.navBorderStyle,{borderBottomColor:'#ffffff',}]} >
                  <Text style={[styles.navItemStyle,{color:'#ffffff'}]} onPress={this.navigateToScreen('PrivacyScreen')}>
                    Privacy Policy
                    </Text>
                </View>
                <View style={[styles.navBorderStyle,{borderBottomColor:'#ffffff',}]} >
                  <Text style={[styles.navItemStyle,{color:'#ffffff'}]} onPress={this.navigateToScreen('AboutArtistScreen')}>
                    About the artist
                  </Text>
                </View>
                <View style={[styles.navBorderStyle,{borderBottomColor:'#ffffff',}]} >
                  <Text style={[styles.navItemStyle,{color:'#ffffff'}]} onPress={this.navigateToScreen('ShareExpScreen')}>
                  Share your experiences
                  </Text>
                </View>
                <View style={[styles.navBorderStyle,{borderBottomColor:'#ffffff',}]} >
                  <Text style={[styles.navItemStyle,{color:'#ffffff'}]} onPress={this.our_website}>
                  Our website
                  </Text>
                </View>

            </View>
            </LinearGradient>
        </ScrollView>

      </View>
    );
  }
}

SideMenu.propTypes = {
  navigation: PropTypes.object
};


export default SideMenu;

const styles = StyleSheet.create({
  container: {
      flex:1,
      backgroundColor: '#386b98'
    },
    navtitle:{
      textAlign:'center',
      fontSize:30,
      fontWeight:'bold',
      color:'#226f9e'
    },
    navItemStyle: {
      padding: 10,
      marginBottom:10,
      textAlign:'left',
      fontSize:15,
      fontWeight:'bold',
      textTransform:'uppercase'
    },
    navBorderStyle: {
       borderBottomWidth:2,
       marginBottom:10
    },
    navSectionStyle: {

      marginTop:15,
      marginLeft:30,
      marginRight:30,

    },
    sectionHeadingStyle: {
      paddingVertical: 10,
      paddingHorizontal: 5
    },
    footerContainer: {
      padding: 20,
      backgroundColor: 'lightgrey'
  }

});
