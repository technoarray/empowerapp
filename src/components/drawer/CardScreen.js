import React, { Component } from 'react';
import {StatusBar,Platform,StyleSheet,Text,View,Image,TouchableOpacity,Alert,BackHandler} from 'react-native';
import Swiper from 'react-native-swiper';
import FlipCard from 'react-native-flip-card';
import { AppStyles, AppSizes, AppColors } from '../../themes/'
import LinearGradient from 'react-native-linear-gradient';
import { NavigationActions } from 'react-navigation';
import Share from 'react-native-share';
import {jsonImages} from '../data/images';

/* Images */
var logo = require( '../../themes/Images/logo.png')
var menu_icon=require( '../../themes/Images/menu_icon_144.png')
var showbtn = require( '../../themes/Images/showbtn_300.png')
var sharebtn = require( '../../themes/Images/sharebtn.png')
var front_card = require( '../../themes/Images/cards/android/front.png')

let _that
let img_arr=[]
img_arr=jsonImages
export default class CardScreen extends Component {
  constructor (props) {
    super(props)
    this.state = {
      flip2: false,
      flip3: false,
      flip4: false,
      items: [],
      array2:[],
      isVisible: false,
      visible: true
    },
      _that = this;
  }

  componentWillMount(){
    let images_arr=[];
    for(var i in jsonImages){
      images_arr[i]=jsonImages[i];
    }

    //BackHandler.addEventListener('hardwareBackPress',this.handleBackButton);

    let array2=[];
    while(images_arr.length !== 0){
      let randomIndex=Math.floor(Math.random() * images_arr.length);
      array2.push(images_arr[randomIndex]);
      images_arr.splice(randomIndex,1);
    }
    this.setState({array2:array2})
  }

  /*handleBackButton = () => {
    Alert.alert(
      'Exit App',
      'Exiting the Application?',[
        {
          text:'Cancel',
          style:'cancel'
        },
        {
          text:'OK',
          onPress:()=> BackHandler.exitApp()
        }
      ],
      {
        cancelable:false
      }
    )
    return true;
  }*/

  //componentWillUnmount(){
  //  BackHandler.removeEventListener('hardwareBackPress',this.handleBackButton)
  //}

  onCancel() {
    console.log("CANCEL")
    _that.setState({visible:false});
  }
  onOpen() {
    console.log("OPEN")
    _that.setState({visible:true});
  }

  shareImage(base64Img){
    shareImageBase64 = {
        title: "Share image",
        message: "http://www.empowerednurses.org/",
        url: base64Img,
        subject: "Share Card"
      }
    Share.open(shareImageBase64).then((res) => { console.log(res) })
    .catch((err) => { err && console.log(err); })
  }

    menu_click() {
    //alert('hello');
    _that.props.navigation.openDrawer();
  }

  render() {
    return (
      <View style={styles.container}>
      <StatusBar hidden={true} />
      <View style={styles.subcontainer}>
      <LinearGradient colors={['#3baeca', '#3baeca', '#3983ab', '#386b98']}>
      <View style={{ width:AppSizes.screen.width,flex:.07,flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', marginTop: 20,marginBottom: 20}}>
          <View style={{ marginTop:0, paddingLeft:10}}>
              <TouchableOpacity onPress={this.menu_click}>
                  <Image source={menu_icon} style={{resizeMode: 'contain', width: 30, height: 30, }} />
              </TouchableOpacity>
          </View>
          <View style={{ marginTop:0}}>
              <Image source={logo} style={{resizeMode: 'contain', height: 200, width: 200}} />
          </View>
          <View style={{ marginTop:0}}>

          </View>
      </View>

      <View style={{flex:.95,width:AppSizes.screen.width}}>
          <Swiper style={styles.wrapper} showsButtons={true} showsPagination={false}
          buttonWrapperStyle={styles.buttonWrapperStyle}
          nextButton={<Text style={styles.swiperbuttonText}>›</Text>}
          prevButton={<Text style={styles.swiperbuttonText}>‹</Text>}
          >


            {/* Card 2 */}
             <View style={styles.slide}>
                <View style={styles.cardflip}>
                   <FlipCard flip={this.state.flip2} style={styles.card} friction={6} perspective={1000} flipHorizontal={true} flipVertical={false} clickable={true} alignHeight={true} onFlipEnd={(isFlipEnd)=>{console.log('isFlipEnd', isFlipEnd)}}>

                    {/* Face Side */}
                    <View style={styles.face}>
                        <Image source={front_card} style={styles.cardImage}  />
                    </View>

                    {/* Back Side */}
                    <View style={styles.back}>
                        <Image source={this.state.array2[0].url} style={styles.cardImage} />
                    </View>
					</FlipCard>
                </View>
                <View style={styles.showbtnview}>
					<TouchableOpacity onPress={()=>{this.setState({flip2: !this.state.flip2})}}  >
                          <Image source={showbtn} style={styles.showbtnImage} />
					</TouchableOpacity>
          <TouchableOpacity onPress={this.shareImage.bind(this,this.state.array2[0].base64)}>
                          <Image source={sharebtn} style={styles.showbtnImage} />
					</TouchableOpacity>
                </View>
            </View>

			{/* Card 3 */}
             <View style={styles.slide}>
                <View style={styles.cardflip}>
                   <FlipCard flip={this.state.flip3} style={styles.card} friction={6} perspective={1000} flipHorizontal={true} flipVertical={false} clickable={true} alignHeight={true} onFlipEnd={(isFlipEnd)=>{console.log('isFlipEnd', isFlipEnd)}}>

                    {/* Face Side */}
                    <View style={styles.face}>
                        <Image source={front_card} style={styles.cardImage}  />
                    </View>

                    {/* Back Side */}
                    <View style={styles.back}>
                        <Image source={this.state.array2[1].url} style={styles.cardImage} />
                    </View>
					</FlipCard>
                </View>
                <View style={styles.showbtnview}>
					<TouchableOpacity onPress={()=>{this.setState({flip3: !this.state.flip3})}}  >
                <Image source={showbtn} style={styles.showbtnImage} />
					</TouchableOpacity>
          <TouchableOpacity onPress={this.shareImage.bind(this,this.state.array2[1].base64)}>
                          <Image source={sharebtn} style={styles.showbtnImage} />
					</TouchableOpacity>
                </View>
            </View>

			{/* Card 4 */}
             <View style={styles.slide}>
                <View style={styles.cardflip}>
                   <FlipCard flip={this.state.flip4} style={styles.card} friction={6} perspective={1000} flipHorizontal={true} flipVertical={false} clickable={true} alignHeight={true} onFlipEnd={(isFlipEnd)=>{console.log('isFlipEnd', isFlipEnd)}}>

                    {/* Face Side */}
                    <View style={styles.face}>
                        <Image source={front_card} style={styles.cardImage}  />
                    </View>

                    {/* Back Side */}
                    <View style={styles.back}>
                        <Image source={this.state.array2[2].url} style={styles.cardImage} />
                    </View>
					</FlipCard>
                </View>
                <View style={styles.showbtnview}>
					<TouchableOpacity onPress={()=>{this.setState({flip4: !this.state.flip4})}}  >
                          <Image source={showbtn} style={styles.showbtnImage} />
					</TouchableOpacity>
          <TouchableOpacity onPress={this.shareImage.bind(this,this.state.array2[2].base64)}>
                          <Image source={sharebtn} style={styles.showbtnImage} />
					</TouchableOpacity>
                </View>
            </View>

			{/* Card 5 */}
             <View style={styles.slide}>
                <View style={styles.cardflip}>
                   <FlipCard flip={this.state.flip5} style={styles.card} friction={6} perspective={1000} flipHorizontal={true} flipVertical={false} clickable={true} alignHeight={true} onFlipEnd={(isFlipEnd)=>{console.log('isFlipEnd', isFlipEnd)}}>

                    {/* Face Side */}
                    <View style={styles.face}>
                        <Image source={front_card} style={styles.cardImage}  />
                    </View>

                    {/* Back Side */}
                    <View style={styles.back}>
                        <Image source={this.state.array2[3].url} style={styles.cardImage} />
                    </View>
					</FlipCard>
                </View>
                <View style={styles.showbtnview}>
					<TouchableOpacity onPress={()=>{this.setState({flip5: !this.state.flip5})}}  >
                          <Image source={showbtn} style={styles.showbtnImage} />
					</TouchableOpacity>
          <TouchableOpacity onPress={this.shareImage.bind(this,this.state.array2[3].base64)}>
                          <Image source={sharebtn} style={styles.showbtnImage} />
					</TouchableOpacity>
                </View>
            </View>

			{/* Card 6 */}
             <View style={styles.slide}>
                <View style={styles.cardflip}>
                   <FlipCard flip={this.state.flip6} style={styles.card} friction={6} perspective={1000} flipHorizontal={true} flipVertical={false} clickable={true} alignHeight={true} onFlipEnd={(isFlipEnd)=>{console.log('isFlipEnd', isFlipEnd)}}>

                    {/* Face Side */}
                    <View style={styles.face}>
                        <Image source={front_card} style={styles.cardImage}  />
                    </View>

                    {/* Back Side */}
                    <View style={styles.back}>
                        <Image source={this.state.array2[4].url} style={styles.cardImage} />
                    </View>
					</FlipCard>
                </View>
                <View style={styles.showbtnview}>
					<TouchableOpacity onPress={()=>{this.setState({flip6: !this.state.flip6})}}  >
                          <Image source={showbtn} style={styles.showbtnImage} />
					</TouchableOpacity>
          <TouchableOpacity onPress={this.shareImage.bind(this,this.state.array2[4].base64)}>
                          <Image source={sharebtn} style={styles.showbtnImage} />
					</TouchableOpacity>
                </View>
            </View>

			{/* Card 7 */}
             <View style={styles.slide}>
                <View style={styles.cardflip}>
                   <FlipCard flip={this.state.flip7} style={styles.card} friction={6} perspective={1000} flipHorizontal={true} flipVertical={false} clickable={true} alignHeight={true} onFlipEnd={(isFlipEnd)=>{console.log('isFlipEnd', isFlipEnd)}}>

                    {/* Face Side */}
                    <View style={styles.face}>
                        <Image source={front_card} style={styles.cardImage}  />
                    </View>

                    {/* Back Side */}
                    <View style={styles.back}>
                        <Image source={this.state.array2[5].url} style={styles.cardImage} />
                    </View>
					</FlipCard>
                </View>
                <View style={styles.showbtnview}>
					<TouchableOpacity onPress={()=>{this.setState({flip7: !this.state.flip7})}}  >
                          <Image source={showbtn} style={styles.showbtnImage} />
					</TouchableOpacity>
          <TouchableOpacity onPress={this.shareImage.bind(this,this.state.array2[5].base64)}>
                          <Image source={sharebtn} style={styles.showbtnImage} />
					</TouchableOpacity>
                </View>
            </View>

			{/* Card 8 */}
             <View style={styles.slide}>
                <View style={styles.cardflip}>
                   <FlipCard flip={this.state.flip8} style={styles.card} friction={6} perspective={1000} flipHorizontal={true} flipVertical={false} clickable={true} alignHeight={true} onFlipEnd={(isFlipEnd)=>{console.log('isFlipEnd', isFlipEnd)}}>

                    {/* Face Side */}
                    <View style={styles.face}>
                        <Image source={front_card} style={styles.cardImage}  />
                    </View>

                    {/* Back Side */}
                    <View style={styles.back}>
                        <Image source={this.state.array2[6].url} style={styles.cardImage} />
                    </View>
					</FlipCard>
                </View>
                <View style={styles.showbtnview}>
					<TouchableOpacity onPress={()=>{this.setState({flip8: !this.state.flip8})}}  >
                          <Image source={showbtn} style={styles.showbtnImage} />
					</TouchableOpacity>
          <TouchableOpacity onPress={this.shareImage.bind(this,this.state.array2[6].base64)}>
                          <Image source={sharebtn} style={styles.showbtnImage} />
					</TouchableOpacity>
                </View>
            </View>

			{/* Card 9 */}
             <View style={styles.slide}>
                <View style={styles.cardflip}>
                   <FlipCard flip={this.state.flip9} style={styles.card} friction={6} perspective={1000} flipHorizontal={true} flipVertical={false} clickable={true} alignHeight={true} onFlipEnd={(isFlipEnd)=>{console.log('isFlipEnd', isFlipEnd)}}>

                    {/* Face Side */}
                    <View style={styles.face}>
                        <Image source={front_card} style={styles.cardImage}  />
                    </View>

                    {/* Back Side */}
                    <View style={styles.back}>
                        <Image source={this.state.array2[7].url} style={styles.cardImage} />
                    </View>
					</FlipCard>
                </View>
                <View style={styles.showbtnview}>
					<TouchableOpacity onPress={()=>{this.setState({flip9: !this.state.flip9})}}  >
                          <Image source={showbtn} style={styles.showbtnImage} />
					</TouchableOpacity>
          <TouchableOpacity onPress={this.shareImage.bind(this,this.state.array2[7].base64)}>
                          <Image source={sharebtn} style={styles.showbtnImage} />
					</TouchableOpacity>
                </View>
            </View>

			{/* Card 10 */}
             <View style={styles.slide}>
                <View style={styles.cardflip}>
                   <FlipCard flip={this.state.flip10} style={styles.card} friction={6} perspective={1000} flipHorizontal={true} flipVertical={false} clickable={true} alignHeight={true} onFlipEnd={(isFlipEnd)=>{console.log('isFlipEnd', isFlipEnd)}}>

                    {/* Face Side */}
                    <View style={styles.face}>
                        <Image source={front_card} style={styles.cardImage}  />
                    </View>

                    {/* Back Side */}
                    <View style={styles.back}>
                        <Image source={this.state.array2[8].url} style={styles.cardImage} />
                    </View>
					</FlipCard>
                </View>
                <View style={styles.showbtnview}>
					<TouchableOpacity onPress={()=>{this.setState({flip10: !this.state.flip10})}}  >
                          <Image source={showbtn} style={styles.showbtnImage} />
					</TouchableOpacity>
          <TouchableOpacity onPress={this.shareImage.bind(this,this.state.array2[8].base64)}>
                          <Image source={sharebtn} style={styles.showbtnImage} />
					</TouchableOpacity>
                </View>
            </View>

			{/* Card 11 */}
             <View style={styles.slide}>
                <View style={styles.cardflip}>
                   <FlipCard flip={this.state.flip11} style={styles.card} friction={6} perspective={1000} flipHorizontal={true} flipVertical={false} clickable={true} alignHeight={true} onFlipEnd={(isFlipEnd)=>{console.log('isFlipEnd', isFlipEnd)}}>

                    {/* Face Side */}
                    <View style={styles.face}>
                        <Image source={front_card} style={styles.cardImage}  />
                    </View>

                    {/* Back Side */}
                    <View style={styles.back}>
                        <Image source={this.state.array2[9].url} style={styles.cardImage} />
                    </View>
					</FlipCard>
                </View>
                <View style={styles.showbtnview}>
					<TouchableOpacity onPress={()=>{this.setState({flip11: !this.state.flip11})}}  >
                          <Image source={showbtn} style={styles.showbtnImage} />
					</TouchableOpacity>
          <TouchableOpacity onPress={this.shareImage.bind(this,this.state.array2[9].base64)}>
                          <Image source={sharebtn} style={styles.showbtnImage} />
					</TouchableOpacity>
                </View>
            </View>

			{/* Card 12 */}
             <View style={styles.slide}>
                <View style={styles.cardflip}>
                   <FlipCard flip={this.state.flip12} style={styles.card} friction={6} perspective={1000} flipHorizontal={true} flipVertical={false} clickable={true} alignHeight={true} onFlipEnd={(isFlipEnd)=>{console.log('isFlipEnd', isFlipEnd)}}>

                    {/* Face Side */}
                    <View style={styles.face}>
                        <Image source={front_card} style={styles.cardImage}  />
                    </View>

                    {/* Back Side */}
                    <View style={styles.back}>
                        <Image source={this.state.array2[10].url} style={styles.cardImage} />
                    </View>
					</FlipCard>
                </View>
                <View style={styles.showbtnview}>
					<TouchableOpacity onPress={()=>{this.setState({flip12: !this.state.flip12})}}  >
                          <Image source={showbtn} style={styles.showbtnImage} />
					</TouchableOpacity>
          <TouchableOpacity onPress={this.shareImage.bind(this,this.state.array2[10].base64)}>
                          <Image source={sharebtn} style={styles.showbtnImage} />
					</TouchableOpacity>
                </View>
            </View>

			{/* Card 13 */}
             <View style={styles.slide}>
                <View style={styles.cardflip}>
                   <FlipCard flip={this.state.flip13} style={styles.card} friction={6} perspective={1000} flipHorizontal={true} flipVertical={false} clickable={true} alignHeight={true} onFlipEnd={(isFlipEnd)=>{console.log('isFlipEnd', isFlipEnd)}}>

                    {/* Face Side */}
                    <View style={styles.face}>
                        <Image source={front_card} style={styles.cardImage}  />
                    </View>

                    {/* Back Side */}
                    <View style={styles.back}>
                        <Image source={this.state.array2[11].url} style={styles.cardImage} />
                    </View>
					</FlipCard>
                </View>
                <View style={styles.showbtnview}>
					<TouchableOpacity onPress={()=>{this.setState({flip13: !this.state.flip13})}}  >
                          <Image source={showbtn} style={styles.showbtnImage} />
					</TouchableOpacity>
          <TouchableOpacity onPress={this.shareImage.bind(this,this.state.array2[11].base64)}>
                          <Image source={sharebtn} style={styles.showbtnImage} />
					</TouchableOpacity>
                </View>
            </View>

			{/* Card 14 */}
             <View style={styles.slide}>
                <View style={styles.cardflip}>
                   <FlipCard flip={this.state.flip14} style={styles.card} friction={6} perspective={1000} flipHorizontal={true} flipVertical={false} clickable={true} alignHeight={true} onFlipEnd={(isFlipEnd)=>{console.log('isFlipEnd', isFlipEnd)}}>

                    {/* Face Side */}
                    <View style={styles.face}>
                        <Image source={front_card} style={styles.cardImage}  />
                    </View>

                    {/* Back Side */}
                    <View style={styles.back}>
                        <Image source={this.state.array2[12].url} style={styles.cardImage} />
                    </View>
					</FlipCard>
                </View>
                <View style={styles.showbtnview}>
					<TouchableOpacity onPress={()=>{this.setState({flip14: !this.state.flip14})}}  >
                          <Image source={showbtn} style={styles.showbtnImage} />
					</TouchableOpacity>
          <TouchableOpacity onPress={this.shareImage.bind(this,this.state.array2[12].base64)}>
                          <Image source={sharebtn} style={styles.showbtnImage} />
					</TouchableOpacity>
                </View>
            </View>

			{/* Card 15 */}
             <View style={styles.slide}>
                <View style={styles.cardflip}>
                   <FlipCard flip={this.state.flip15} style={styles.card} friction={6} perspective={1000} flipHorizontal={true} flipVertical={false} clickable={true} alignHeight={true} onFlipEnd={(isFlipEnd)=>{console.log('isFlipEnd', isFlipEnd)}}>

                    {/* Face Side */}
                    <View style={styles.face}>
                        <Image source={front_card} style={styles.cardImage}  />
                    </View>

                    {/* Back Side */}
                    <View style={styles.back}>
                        <Image source={this.state.array2[13].url} style={styles.cardImage} />
                    </View>
					</FlipCard>
                </View>
                <View style={styles.showbtnview}>
					<TouchableOpacity onPress={()=>{this.setState({flip15: !this.state.flip15})}}  >
                          <Image source={showbtn} style={styles.showbtnImage} />
					</TouchableOpacity>
          <TouchableOpacity onPress={this.shareImage.bind(this,this.state.array2[13].base64)}>
                          <Image source={sharebtn} style={styles.showbtnImage} />
					</TouchableOpacity>
                </View>
            </View>
			{/* Card 16 */}
             <View style={styles.slide}>
                <View style={styles.cardflip}>
                   <FlipCard flip={this.state.flip16} style={styles.card} friction={6} perspective={1000} flipHorizontal={true} flipVertical={false} clickable={true} alignHeight={true} onFlipEnd={(isFlipEnd)=>{console.log('isFlipEnd', isFlipEnd)}}>

                    {/* Face Side */}
                    <View style={styles.face}>
                        <Image source={front_card} style={styles.cardImage}  />
                    </View>

                    {/* Back Side */}
                    <View style={styles.back}>
                        <Image source={this.state.array2[14].url} style={styles.cardImage} />
                    </View>
					</FlipCard>
                </View>
                <View style={styles.showbtnview}>
					<TouchableOpacity onPress={()=>{this.setState({flip16: !this.state.flip16})}}  >
                          <Image source={showbtn} style={styles.showbtnImage} />
					</TouchableOpacity>
          <TouchableOpacity onPress={this.shareImage.bind(this,this.state.array2[14].base64)}>
                          <Image source={sharebtn} style={styles.showbtnImage} />
					</TouchableOpacity>
                </View>
            </View>
			{/* Card 17 */}
             <View style={styles.slide}>
                <View style={styles.cardflip}>
                   <FlipCard flip={this.state.flip17} style={styles.card} friction={6} perspective={1000} flipHorizontal={true} flipVertical={false} clickable={true} alignHeight={true} onFlipEnd={(isFlipEnd)=>{console.log('isFlipEnd', isFlipEnd)}}>

                    {/* Face Side */}
                    <View style={styles.face}>
                        <Image source={front_card} style={styles.cardImage}  />
                    </View>

                    {/* Back Side */}
                    <View style={styles.back}>
                        <Image source={this.state.array2[15].url} style={styles.cardImage} />
                    </View>
					</FlipCard>
                </View>
                <View style={styles.showbtnview}>
					<TouchableOpacity onPress={()=>{this.setState({flip17: !this.state.flip17})}}  >
                          <Image source={showbtn} style={styles.showbtnImage} />
					</TouchableOpacity>
          <TouchableOpacity onPress={this.shareImage.bind(this,this.state.array2[15].base64)}>
                          <Image source={sharebtn} style={styles.showbtnImage} />
					</TouchableOpacity>
                </View>
            </View>
			{/* Card 18 */}
             <View style={styles.slide}>
                <View style={styles.cardflip}>
                   <FlipCard flip={this.state.flip18} style={styles.card} friction={6} perspective={1000} flipHorizontal={true} flipVertical={false} clickable={true} alignHeight={true} onFlipEnd={(isFlipEnd)=>{console.log('isFlipEnd', isFlipEnd)}}>

                    {/* Face Side */}
                    <View style={styles.face}>
                        <Image source={front_card} style={styles.cardImage}  />
                    </View>

                    {/* Back Side */}
                    <View style={styles.back}>
                        <Image source={this.state.array2[16].url} style={styles.cardImage} />
                    </View>
					</FlipCard>
                </View>
                <View style={styles.showbtnview}>
					<TouchableOpacity onPress={()=>{this.setState({flip18: !this.state.flip18})}}  >
                          <Image source={showbtn} style={styles.showbtnImage} />
					</TouchableOpacity>
          <TouchableOpacity onPress={this.shareImage.bind(this,this.state.array2[16].base64)}>
                          <Image source={sharebtn} style={styles.showbtnImage} />
					</TouchableOpacity>
                </View>
            </View>
			{/* Card 19 */}
             <View style={styles.slide}>
                <View style={styles.cardflip}>
                   <FlipCard flip={this.state.flip19} style={styles.card} friction={6} perspective={1000} flipHorizontal={true} flipVertical={false} clickable={true} alignHeight={true} onFlipEnd={(isFlipEnd)=>{console.log('isFlipEnd', isFlipEnd)}}>

                    {/* Face Side */}
                    <View style={styles.face}>
                        <Image source={front_card} style={styles.cardImage}  />
                    </View>

                    {/* Back Side */}
                    <View style={styles.back}>
                        <Image source={this.state.array2[17].url} style={styles.cardImage} />
                    </View>
					</FlipCard>
                </View>
                <View style={styles.showbtnview}>
					<TouchableOpacity onPress={()=>{this.setState({flip19: !this.state.flip19})}}  >
                          <Image source={showbtn} style={styles.showbtnImage} />
					</TouchableOpacity>
          <TouchableOpacity onPress={this.shareImage.bind(this,this.state.array2[17].base64)}>
                          <Image source={sharebtn} style={styles.showbtnImage} />
					</TouchableOpacity>
                </View>
            </View>
			{/* Card 20 */}
      <View style={styles.slide}>
         <View style={styles.cardflip}>
            <FlipCard flip={this.state.flip20} style={styles.card} friction={6} perspective={1000} flipHorizontal={true} flipVertical={false} clickable={true} alignHeight={true} onFlipEnd={(isFlipEnd)=>{console.log('isFlipEnd', isFlipEnd)}}>

             {/* Face Side */}
             <View style={styles.face}>
                 <Image source={front_card} style={styles.cardImage}  />
             </View>

             {/* Back Side */}
             <View style={styles.back}>
                 <Image source={this.state.array2[18].url} style={styles.cardImage} />
             </View>
      </FlipCard>
         </View>
         <View style={styles.showbtnview}>
      <TouchableOpacity onPress={()=>{this.setState({flip20: !this.state.flip20})}}  >
                   <Image source={showbtn} style={styles.showbtnImage} />
      </TouchableOpacity>
      <TouchableOpacity onPress={this.shareImage.bind(this,this.state.array2[18].base64)}>
                      <Image source={sharebtn} style={styles.showbtnImage} />
      </TouchableOpacity>
         </View>
      </View>
      {/* Card 21 */}
      <View style={styles.slide}>
         <View style={styles.cardflip}>
            <FlipCard flip={this.state.flip21} style={styles.card} friction={6} perspective={1000} flipHorizontal={true} flipVertical={false} clickable={true} alignHeight={true} onFlipEnd={(isFlipEnd)=>{console.log('isFlipEnd', isFlipEnd)}}>

             {/* Face Side */}
             <View style={styles.face}>
                 <Image source={front_card} style={styles.cardImage}  />
             </View>

             {/* Back Side */}
             <View style={styles.back}>
                 <Image source={this.state.array2[19].url} style={styles.cardImage} />
             </View>
      </FlipCard>
         </View>
         <View style={styles.showbtnview}>
      <TouchableOpacity onPress={()=>{this.setState({flip21: !this.state.flip21})}}  >
                   <Image source={showbtn} style={styles.showbtnImage} />
      </TouchableOpacity>
      <TouchableOpacity onPress={this.shareImage.bind(this,this.state.array2[19].base64)}>
                      <Image source={sharebtn} style={styles.showbtnImage} />
      </TouchableOpacity>
         </View>
      </View>
      {/* Card 22 */}
      <View style={styles.slide}>
         <View style={styles.cardflip}>
            <FlipCard flip={this.state.flip22} style={styles.card} friction={6} perspective={1000} flipHorizontal={true} flipVertical={false} clickable={true} alignHeight={true} onFlipEnd={(isFlipEnd)=>{console.log('isFlipEnd', isFlipEnd)}}>

             {/* Face Side */}
             <View style={styles.face}>
                 <Image source={front_card} style={styles.cardImage}  />
             </View>

             {/* Back Side */}
             <View style={styles.back}>
                 <Image source={this.state.array2[21].url} style={styles.cardImage} />
             </View>
      </FlipCard>
         </View>
         <View style={styles.showbtnview}>
      <TouchableOpacity onPress={()=>{this.setState({flip22: !this.state.flip22})}}  >
                   <Image source={showbtn} style={styles.showbtnImage} />
      </TouchableOpacity>
      <TouchableOpacity onPress={this.shareImage.bind(this,this.state.array2[21].base64)}>
                      <Image source={sharebtn} style={styles.showbtnImage} />
      </TouchableOpacity>
         </View>
      </View>
      {/* Card 23 */}
      <View style={styles.slide}>
         <View style={styles.cardflip}>
            <FlipCard flip={this.state.flip23} style={styles.card} friction={6} perspective={1000} flipHorizontal={true} flipVertical={false} clickable={true} alignHeight={true} onFlipEnd={(isFlipEnd)=>{console.log('isFlipEnd', isFlipEnd)}}>

             {/* Face Side */}
             <View style={styles.face}>
                 <Image source={front_card} style={styles.cardImage}  />
             </View>

             {/* Back Side */}
             <View style={styles.back}>
                 <Image source={this.state.array2[21].url} style={styles.cardImage} />
             </View>
      </FlipCard>
         </View>
         <View style={styles.showbtnview}>
      <TouchableOpacity onPress={()=>{this.setState({flip23: !this.state.flip23})}}  >
                   <Image source={showbtn} style={styles.showbtnImage} />
      </TouchableOpacity>
      <TouchableOpacity onPress={this.shareImage.bind(this,this.state.array2[21].base64)}>
                      <Image source={sharebtn} style={styles.showbtnImage} />
      </TouchableOpacity>
         </View>
      </View>
      {/* Card 24 */}
      <View style={styles.slide}>
         <View style={styles.cardflip}>
            <FlipCard flip={this.state.flip24} style={styles.card} friction={6} perspective={1000} flipHorizontal={true} flipVertical={false} clickable={true} alignHeight={true} onFlipEnd={(isFlipEnd)=>{console.log('isFlipEnd', isFlipEnd)}}>

             {/* Face Side */}
             <View style={styles.face}>
                 <Image source={front_card} style={styles.cardImage}  />
             </View>

             {/* Back Side */}
             <View style={styles.back}>
                 <Image source={this.state.array2[22].url} style={styles.cardImage} />
             </View>
      </FlipCard>
         </View>
         <View style={styles.showbtnview}>
      <TouchableOpacity onPress={()=>{this.setState({flip24: !this.state.flip24})}}  >
                   <Image source={showbtn} style={styles.showbtnImage} />
      </TouchableOpacity>
      <TouchableOpacity onPress={this.shareImage.bind(this,this.state.array2[22].base64)}>
                      <Image source={sharebtn} style={styles.showbtnImage} />
      </TouchableOpacity>
         </View>
      </View>
      {/* Card 25 */}
      <View style={styles.slide}>
         <View style={styles.cardflip}>
            <FlipCard flip={this.state.flip25} style={styles.card} friction={6} perspective={1000} flipHorizontal={true} flipVertical={false} clickable={true} alignHeight={true} onFlipEnd={(isFlipEnd)=>{console.log('isFlipEnd', isFlipEnd)}}>

             {/* Face Side */}
             <View style={styles.face}>
                 <Image source={front_card} style={styles.cardImage}  />
             </View>

             {/* Back Side */}
             <View style={styles.back}>
                 <Image source={this.state.array2[23].url} style={styles.cardImage} />
             </View>
      </FlipCard>
         </View>
         <View style={styles.showbtnview}>
      <TouchableOpacity onPress={()=>{this.setState({flip25: !this.state.flip25})}}  >
                   <Image source={showbtn} style={styles.showbtnImage} />
      </TouchableOpacity>
      <TouchableOpacity onPress={this.shareImage.bind(this,this.state.array2[23].base64)}>
                      <Image source={sharebtn} style={styles.showbtnImage} />
      </TouchableOpacity>
         </View>
      </View>
      {/* Card 26 */}
      <View style={styles.slide}>
         <View style={styles.cardflip}>
            <FlipCard flip={this.state.flip26} style={styles.card} friction={6} perspective={1000} flipHorizontal={true} flipVertical={false} clickable={true} alignHeight={true} onFlipEnd={(isFlipEnd)=>{console.log('isFlipEnd', isFlipEnd)}}>

             {/* Face Side */}
             <View style={styles.face}>
                 <Image source={front_card} style={styles.cardImage}  />
             </View>

             {/* Back Side */}
             <View style={styles.back}>
                 <Image source={this.state.array2[24].url} style={styles.cardImage} />
             </View>
      </FlipCard>
         </View>
         <View style={styles.showbtnview}>
      <TouchableOpacity onPress={()=>{this.setState({flip26: !this.state.flip26})}}  >
                   <Image source={showbtn} style={styles.showbtnImage} />
      </TouchableOpacity>
      <TouchableOpacity onPress={this.shareImage.bind(this,this.state.array2[24].base64)}>
                      <Image source={sharebtn} style={styles.showbtnImage} />
      </TouchableOpacity>
         </View>
      </View>
      {/* Card 27 */}
      <View style={styles.slide}>
         <View style={styles.cardflip}>
            <FlipCard flip={this.state.flip27} style={styles.card} friction={6} perspective={1000} flipHorizontal={true} flipVertical={false} clickable={true} alignHeight={true} onFlipEnd={(isFlipEnd)=>{console.log('isFlipEnd', isFlipEnd)}}>

             {/* Face Side */}
             <View style={styles.face}>
                 <Image source={front_card} style={styles.cardImage}  />
             </View>

             {/* Back Side */}
             <View style={styles.back}>
                 <Image source={this.state.array2[25].url} style={styles.cardImage} />
             </View>
      </FlipCard>
         </View>
         <View style={styles.showbtnview}>
      <TouchableOpacity onPress={()=>{this.setState({flip27: !this.state.flip27})}}  >
                   <Image source={showbtn} style={styles.showbtnImage} />
      </TouchableOpacity>
      <TouchableOpacity onPress={this.shareImage.bind(this,this.state.array2[25].base64)}>
                      <Image source={sharebtn} style={styles.showbtnImage} />
      </TouchableOpacity>
         </View>
      </View>
      {/* Card 28 */}
      <View style={styles.slide}>
         <View style={styles.cardflip}>
            <FlipCard flip={this.state.flip28} style={styles.card} friction={6} perspective={1000} flipHorizontal={true} flipVertical={false} clickable={true} alignHeight={true} onFlipEnd={(isFlipEnd)=>{console.log('isFlipEnd', isFlipEnd)}}>

             {/* Face Side */}
             <View style={styles.face}>
                 <Image source={front_card} style={styles.cardImage}  />
             </View>

             {/* Back Side */}
             <View style={styles.back}>
                 <Image source={this.state.array2[26].url} style={styles.cardImage} />
             </View>
      </FlipCard>
         </View>
         <View style={styles.showbtnview}>
      <TouchableOpacity onPress={()=>{this.setState({flip28: !this.state.flip28})}}  >
                   <Image source={showbtn} style={styles.showbtnImage} />
      </TouchableOpacity>
      <TouchableOpacity onPress={this.shareImage.bind(this,this.state.array2[26].base64)}>
                      <Image source={sharebtn} style={styles.showbtnImage} />
      </TouchableOpacity>
         </View>
      </View>
      {/* Card 29 */}
      <View style={styles.slide}>
         <View style={styles.cardflip}>
            <FlipCard flip={this.state.flip29} style={styles.card} friction={6} perspective={1000} flipHorizontal={true} flipVertical={false} clickable={true} alignHeight={true} onFlipEnd={(isFlipEnd)=>{console.log('isFlipEnd', isFlipEnd)}}>

             {/* Face Side */}
             <View style={styles.face}>
                 <Image source={front_card} style={styles.cardImage}  />
             </View>

             {/* Back Side */}
             <View style={styles.back}>
                 <Image source={this.state.array2[27].url} style={styles.cardImage} />
             </View>
      </FlipCard>
         </View>
         <View style={styles.showbtnview}>
      <TouchableOpacity onPress={()=>{this.setState({flip29: !this.state.flip29})}}  >
                   <Image source={showbtn} style={styles.showbtnImage} />
      </TouchableOpacity>
      <TouchableOpacity onPress={this.shareImage.bind(this,this.state.array2[27].base64)}>
                      <Image source={sharebtn} style={styles.showbtnImage} />
      </TouchableOpacity>
         </View>
      </View>
      {/* Card 30 */}
      <View style={styles.slide}>
         <View style={styles.cardflip}>
            <FlipCard flip={this.state.flip30} style={styles.card} friction={6} perspective={1000} flipHorizontal={true} flipVertical={false} clickable={true} alignHeight={true} onFlipEnd={(isFlipEnd)=>{console.log('isFlipEnd', isFlipEnd)}}>

             {/* Face Side */}
             <View style={styles.face}>
                 <Image source={front_card} style={styles.cardImage}  />
             </View>

             {/* Back Side */}
             <View style={styles.back}>
                 <Image source={this.state.array2[28].url} style={styles.cardImage} />
             </View>
      </FlipCard>
         </View>
         <View style={styles.showbtnview}>
      <TouchableOpacity onPress={()=>{this.setState({flip30: !this.state.flip30})}}  >
                   <Image source={showbtn} style={styles.showbtnImage} />
      </TouchableOpacity>
      <TouchableOpacity onPress={this.shareImage.bind(this,this.state.array2[28].base64)}>
                      <Image source={sharebtn} style={styles.showbtnImage} />
      </TouchableOpacity>
         </View>
      </View>
      {/* Card 31 */}
      <View style={styles.slide}>
         <View style={styles.cardflip}>
            <FlipCard flip={this.state.flip31} style={styles.card} friction={6} perspective={1000} flipHorizontal={true} flipVertical={false} clickable={true} alignHeight={true} onFlipEnd={(isFlipEnd)=>{console.log('isFlipEnd', isFlipEnd)}}>

             {/* Face Side */}
             <View style={styles.face}>
                 <Image source={front_card} style={styles.cardImage}  />
             </View>

             {/* Back Side */}
             <View style={styles.back}>
                 <Image source={this.state.array2[29].url} style={styles.cardImage} />
             </View>
      </FlipCard>
         </View>
         <View style={styles.showbtnview}>
      <TouchableOpacity onPress={()=>{this.setState({flip31: !this.state.flip31})}}  >
                   <Image source={showbtn} style={styles.showbtnImage} />
      </TouchableOpacity>
      <TouchableOpacity onPress={this.shareImage.bind(this,this.state.array2[29].base64)}>
                      <Image source={sharebtn} style={styles.showbtnImage} />
      </TouchableOpacity>
         </View>
      </View>
      {/* Card 32 */}
      <View style={styles.slide}>
         <View style={styles.cardflip}>
            <FlipCard flip={this.state.flip32} style={styles.card} friction={6} perspective={1000} flipHorizontal={true} flipVertical={false} clickable={true} alignHeight={true} onFlipEnd={(isFlipEnd)=>{console.log('isFlipEnd', isFlipEnd)}}>

             {/* Face Side */}
             <View style={styles.face}>
                 <Image source={front_card} style={styles.cardImage}  />
             </View>

             {/* Back Side */}
             <View style={styles.back}>
                 <Image source={this.state.array2[30].url} style={styles.cardImage} />
             </View>
      </FlipCard>
         </View>
         <View style={styles.showbtnview}>
      <TouchableOpacity onPress={()=>{this.setState({flip32: !this.state.flip32})}}  >
                   <Image source={showbtn} style={styles.showbtnImage} />
      </TouchableOpacity>
      <TouchableOpacity onPress={this.shareImage.bind(this,this.state.array2[30].base64)}>
                      <Image source={sharebtn} style={styles.showbtnImage} />
      </TouchableOpacity>
         </View>
      </View>
      {/* Card 33 */}
      <View style={styles.slide}>
         <View style={styles.cardflip}>
            <FlipCard flip={this.state.flip33} style={styles.card} friction={6} perspective={1000} flipHorizontal={true} flipVertical={false} clickable={true} alignHeight={true} onFlipEnd={(isFlipEnd)=>{console.log('isFlipEnd', isFlipEnd)}}>

             {/* Face Side */}
             <View style={styles.face}>
                 <Image source={front_card} style={styles.cardImage}  />
             </View>

             {/* Back Side */}
             <View style={styles.back}>
                 <Image source={this.state.array2[31].url} style={styles.cardImage} />
             </View>
      </FlipCard>
         </View>
         <View style={styles.showbtnview}>
      <TouchableOpacity onPress={()=>{this.setState({flip33: !this.state.flip33})}}  >
                   <Image source={showbtn} style={styles.showbtnImage} />
      </TouchableOpacity>
      <TouchableOpacity onPress={this.shareImage.bind(this,this.state.array2[31].base64)}>
                      <Image source={sharebtn} style={styles.showbtnImage} />
      </TouchableOpacity>
         </View>
      </View>
      {/* Card 34 */}
      <View style={styles.slide}>
         <View style={styles.cardflip}>
            <FlipCard flip={this.state.flip34} style={styles.card} friction={6} perspective={1000} flipHorizontal={true} flipVertical={false} clickable={true} alignHeight={true} onFlipEnd={(isFlipEnd)=>{console.log('isFlipEnd', isFlipEnd)}}>

             {/* Face Side */}
             <View style={styles.face}>
                 <Image source={front_card} style={styles.cardImage}  />
             </View>

             {/* Back Side */}
             <View style={styles.back}>
                 <Image source={this.state.array2[32].url} style={styles.cardImage} />
             </View>
      </FlipCard>
         </View>
         <View style={styles.showbtnview}>
      <TouchableOpacity onPress={()=>{this.setState({flip34: !this.state.flip34})}}  >
                   <Image source={showbtn} style={styles.showbtnImage} />
      </TouchableOpacity>
      <TouchableOpacity onPress={this.shareImage.bind(this,this.state.array2[32].base64)}>
                      <Image source={sharebtn} style={styles.showbtnImage} />
      </TouchableOpacity>
         </View>
      </View>
      {/* Card 35 */}
      <View style={styles.slide}>
         <View style={styles.cardflip}>
            <FlipCard flip={this.state.flip35} style={styles.card} friction={6} perspective={1000} flipHorizontal={true} flipVertical={false} clickable={true} alignHeight={true} onFlipEnd={(isFlipEnd)=>{console.log('isFlipEnd', isFlipEnd)}}>

             {/* Face Side */}
             <View style={styles.face}>
                 <Image source={front_card} style={styles.cardImage}  />
             </View>

             {/* Back Side */}
             <View style={styles.back}>
                 <Image source={this.state.array2[33].url} style={styles.cardImage} />
             </View>
      </FlipCard>
         </View>
         <View style={styles.showbtnview}>
      <TouchableOpacity onPress={()=>{this.setState({flip35: !this.state.flip35})}}  >
                   <Image source={showbtn} style={styles.showbtnImage} />
      </TouchableOpacity>
      <TouchableOpacity onPress={this.shareImage.bind(this,this.state.array2[33].base64)}>
                      <Image source={sharebtn} style={styles.showbtnImage} />
      </TouchableOpacity>
         </View>
      </View>
      {/* Card 36 */}
      <View style={styles.slide}>
         <View style={styles.cardflip}>
            <FlipCard flip={this.state.flip36} style={styles.card} friction={6} perspective={1000} flipHorizontal={true} flipVertical={false} clickable={true} alignHeight={true} onFlipEnd={(isFlipEnd)=>{console.log('isFlipEnd', isFlipEnd)}}>

             {/* Face Side */}
             <View style={styles.face}>
                 <Image source={front_card} style={styles.cardImage}  />
             </View>

             {/* Back Side */}
             <View style={styles.back}>
                 <Image source={this.state.array2[34].url} style={styles.cardImage} />
             </View>
      </FlipCard>
         </View>
         <View style={styles.showbtnview}>
      <TouchableOpacity onPress={()=>{this.setState({flip36: !this.state.flip36})}}  >
                   <Image source={showbtn} style={styles.showbtnImage} />
      </TouchableOpacity>
      <TouchableOpacity onPress={this.shareImage.bind(this,this.state.array2[34].base64)}>
                      <Image source={sharebtn} style={styles.showbtnImage} />
      </TouchableOpacity>
         </View>
      </View>
      {/* Card 37 */}
      <View style={styles.slide}>
         <View style={styles.cardflip}>
            <FlipCard flip={this.state.flip37} style={styles.card} friction={6} perspective={1000} flipHorizontal={true} flipVertical={false} clickable={true} alignHeight={true} onFlipEnd={(isFlipEnd)=>{console.log('isFlipEnd', isFlipEnd)}}>

             {/* Face Side */}
             <View style={styles.face}>
                 <Image source={front_card} style={styles.cardImage}  />
             </View>

             {/* Back Side */}
             <View style={styles.back}>
                 <Image source={this.state.array2[35].url} style={styles.cardImage} />
             </View>
      </FlipCard>
         </View>
         <View style={styles.showbtnview}>
      <TouchableOpacity onPress={()=>{this.setState({flip37: !this.state.flip37})}}  >
                   <Image source={showbtn} style={styles.showbtnImage} />
      </TouchableOpacity>
      <TouchableOpacity onPress={this.shareImage.bind(this,this.state.array2[35].base64)}>
                      <Image source={sharebtn} style={styles.showbtnImage} />
      </TouchableOpacity>
         </View>
      </View>
      {/* Card 38 */}
      <View style={styles.slide}>
         <View style={styles.cardflip}>
            <FlipCard flip={this.state.flip38} style={styles.card} friction={6} perspective={1000} flipHorizontal={true} flipVertical={false} clickable={true} alignHeight={true} onFlipEnd={(isFlipEnd)=>{console.log('isFlipEnd', isFlipEnd)}}>

             {/* Face Side */}
             <View style={styles.face}>
                 <Image source={front_card} style={styles.cardImage}  />
             </View>

             {/* Back Side */}
             <View style={styles.back}>
                 <Image source={this.state.array2[36].url} style={styles.cardImage} />
             </View>
      </FlipCard>
         </View>
         <View style={styles.showbtnview}>
      <TouchableOpacity onPress={()=>{this.setState({flip38: !this.state.flip38})}}  >
                   <Image source={showbtn} style={styles.showbtnImage} />
      </TouchableOpacity>
      <TouchableOpacity onPress={this.shareImage.bind(this,this.state.array2[36].base64)}>
                      <Image source={sharebtn} style={styles.showbtnImage} />
      </TouchableOpacity>
         </View>
      </View>
      {/* Card 39 */}
      <View style={styles.slide}>
         <View style={styles.cardflip}>
            <FlipCard flip={this.state.flip39} style={styles.card} friction={6} perspective={1000} flipHorizontal={true} flipVertical={false} clickable={true} alignHeight={true} onFlipEnd={(isFlipEnd)=>{console.log('isFlipEnd', isFlipEnd)}}>

             {/* Face Side */}
             <View style={styles.face}>
                 <Image source={front_card} style={styles.cardImage}  />
             </View>

             {/* Back Side */}
             <View style={styles.back}>
                 <Image source={this.state.array2[37].url} style={styles.cardImage} />
             </View>
      </FlipCard>
         </View>
         <View style={styles.showbtnview}>
      <TouchableOpacity onPress={()=>{this.setState({flip39: !this.state.flip39})}}  >
                   <Image source={showbtn} style={styles.showbtnImage} />
      </TouchableOpacity>
      <TouchableOpacity onPress={this.shareImage.bind(this,this.state.array2[37].base64)}>
                      <Image source={sharebtn} style={styles.showbtnImage} />
      </TouchableOpacity>
         </View>
      </View>
      {/* Card 40 */}
      <View style={styles.slide}>
         <View style={styles.cardflip}>
            <FlipCard flip={this.state.flip40} style={styles.card} friction={6} perspective={1000} flipHorizontal={true} flipVertical={false} clickable={true} alignHeight={true} onFlipEnd={(isFlipEnd)=>{console.log('isFlipEnd', isFlipEnd)}}>

             {/* Face Side */}
             <View style={styles.face}>
                 <Image source={front_card} style={styles.cardImage}  />
             </View>

             {/* Back Side */}
             <View style={styles.back}>
                 <Image source={this.state.array2[38].url} style={styles.cardImage} />
             </View>
      </FlipCard>
         </View>
         <View style={styles.showbtnview}>
      <TouchableOpacity onPress={()=>{this.setState({flip40: !this.state.flip40})}}  >
                   <Image source={showbtn} style={styles.showbtnImage} />
      </TouchableOpacity>
      <TouchableOpacity onPress={this.shareImage.bind(this,this.state.array2[38].base64)}>
                      <Image source={sharebtn} style={styles.showbtnImage} />
      </TouchableOpacity>
         </View>
      </View>
      {/* Card 41 */}
            <View style={styles.slide}>
               <View style={styles.cardflip}>
                  <FlipCard flip={this.state.flip41} style={styles.card} friction={6} perspective={1000} flipHorizontal={true} flipVertical={false} clickable={true} alignHeight={true} onFlipEnd={(isFlipEnd)=>{console.log('isFlipEnd', isFlipEnd)}}>

                   {/* Face Side */}
                   <View style={styles.face}>
                       <Image source={front_card} style={styles.cardImage}  />
                   </View>

                   {/* Back Side */}
                   <View style={styles.back}>
                       <Image source={this.state.array2[39].url} style={styles.cardImage} />
                   </View>
         </FlipCard>
               </View>
               <View style={styles.showbtnview}>
         <TouchableOpacity onPress={()=>{this.setState({flip56: !this.state.flip41})}}  >
                         <Image source={showbtn} style={styles.showbtnImage} />
         </TouchableOpacity>
         <TouchableOpacity onPress={this.shareImage.bind(this,this.state.array2[39].base64)}>
                         <Image source={sharebtn} style={styles.showbtnImage} />
         </TouchableOpacity>
               </View>
           </View>



      {/* Card 42 */}
      <View style={styles.slide}>
         <View style={styles.cardflip}>
            <FlipCard flip={this.state.flip42} style={styles.card} friction={6} perspective={1000} flipHorizontal={true} flipVertical={false} clickable={true} alignHeight={true} onFlipEnd={(isFlipEnd)=>{console.log('isFlipEnd', isFlipEnd)}}>

             {/* Face Side */}
             <View style={styles.face}>
                 <Image source={front_card} style={styles.cardImage}  />
             </View>

             {/* Back Side */}
             <View style={styles.back}>
                 <Image source={this.state.array2[40].url} style={styles.cardImage} />
             </View>
      </FlipCard>
         </View>
         <View style={styles.showbtnview}>
      <TouchableOpacity onPress={()=>{this.setState({flip42: !this.state.flip42})}}  >
                   <Image source={showbtn} style={styles.showbtnImage} />
      </TouchableOpacity>
      <TouchableOpacity onPress={this.shareImage.bind(this,this.state.array2[40].base64)}>
                      <Image source={sharebtn} style={styles.showbtnImage} />
      </TouchableOpacity>
         </View>
      </View>
      {/* Card 43 */}
      <View style={styles.slide}>
         <View style={styles.cardflip}>
            <FlipCard flip={this.state.flip43} style={styles.card} friction={6} perspective={1000} flipHorizontal={true} flipVertical={false} clickable={true} alignHeight={true} onFlipEnd={(isFlipEnd)=>{console.log('isFlipEnd', isFlipEnd)}}>

             {/* Face Side */}
             <View style={styles.face}>
                 <Image source={front_card} style={styles.cardImage}  />
             </View>

             {/* Back Side */}
             <View style={styles.back}>
                 <Image source={this.state.array2[41].url} style={styles.cardImage} />
             </View>
      </FlipCard>
         </View>
         <View style={styles.showbtnview}>
      <TouchableOpacity onPress={()=>{this.setState({flip43: !this.state.flip43})}}  >
                   <Image source={showbtn} style={styles.showbtnImage} />
      </TouchableOpacity>
      <TouchableOpacity onPress={this.shareImage.bind(this,this.state.array2[41].base64)}>
                      <Image source={sharebtn} style={styles.showbtnImage} />
      </TouchableOpacity>
         </View>
      </View>
      {/* Card 44 */}
      <View style={styles.slide}>
         <View style={styles.cardflip}>
            <FlipCard flip={this.state.flip44} style={styles.card} friction={6} perspective={1000} flipHorizontal={true} flipVertical={false} clickable={true} alignHeight={true} onFlipEnd={(isFlipEnd)=>{console.log('isFlipEnd', isFlipEnd)}}>

             {/* Face Side */}
             <View style={styles.face}>
                 <Image source={front_card} style={styles.cardImage}  />
             </View>

             {/* Back Side */}
             <View style={styles.back}>
                 <Image source={this.state.array2[42].url} style={styles.cardImage} />
             </View>
      </FlipCard>
         </View>
         <View style={styles.showbtnview}>
      <TouchableOpacity onPress={()=>{this.setState({flip44: !this.state.flip44})}}  >
                   <Image source={showbtn} style={styles.showbtnImage} />
      </TouchableOpacity>
      <TouchableOpacity onPress={this.shareImage.bind(this,this.state.array2[42].base64)}>
                      <Image source={sharebtn} style={styles.showbtnImage} />
      </TouchableOpacity>
         </View>
      </View>
      {/* Card 45 */}
      <View style={styles.slide}>
         <View style={styles.cardflip}>
            <FlipCard flip={this.state.flip45} style={styles.card} friction={6} perspective={1000} flipHorizontal={true} flipVertical={false} clickable={true} alignHeight={true} onFlipEnd={(isFlipEnd)=>{console.log('isFlipEnd', isFlipEnd)}}>

             {/* Face Side */}
             <View style={styles.face}>
                 <Image source={front_card} style={styles.cardImage}  />
             </View>

             {/* Back Side */}
             <View style={styles.back}>
                 <Image source={this.state.array2[43].url} style={styles.cardImage} />
             </View>
      </FlipCard>
         </View>
         <View style={styles.showbtnview}>
      <TouchableOpacity onPress={()=>{this.setState({flip45: !this.state.flip45})}}  >
                   <Image source={showbtn} style={styles.showbtnImage} />
      </TouchableOpacity>
      <TouchableOpacity onPress={this.shareImage.bind(this,this.state.array2[43].base64)}>
                      <Image source={sharebtn} style={styles.showbtnImage} />
      </TouchableOpacity>
         </View>
      </View>
      {/* Card 46 */}
      <View style={styles.slide}>
         <View style={styles.cardflip}>
            <FlipCard flip={this.state.flip46} style={styles.card} friction={6} perspective={1000} flipHorizontal={true} flipVertical={false} clickable={true} alignHeight={true} onFlipEnd={(isFlipEnd)=>{console.log('isFlipEnd', isFlipEnd)}}>

             {/* Face Side */}
             <View style={styles.face}>
                 <Image source={front_card} style={styles.cardImage}  />
             </View>

             {/* Back Side */}
             <View style={styles.back}>
                 <Image source={this.state.array2[44].url} style={styles.cardImage} />
             </View>
      </FlipCard>
         </View>
         <View style={styles.showbtnview}>
      <TouchableOpacity onPress={()=>{this.setState({flip46: !this.state.flip46})}}  >
                   <Image source={showbtn} style={styles.showbtnImage} />
      </TouchableOpacity>
      <TouchableOpacity onPress={this.shareImage.bind(this,this.state.array2[44].base64)}>
                      <Image source={sharebtn} style={styles.showbtnImage} />
      </TouchableOpacity>
         </View>
      </View>

      {/* Card 47 */}
      <View style={styles.slide}>
         <View style={styles.cardflip}>
            <FlipCard flip={this.state.flip47} style={styles.card} friction={6} perspective={1000} flipHorizontal={true} flipVertical={false} clickable={true} alignHeight={true} onFlipEnd={(isFlipEnd)=>{console.log('isFlipEnd', isFlipEnd)}}>

             {/* Face Side */}
             <View style={styles.face}>
                 <Image source={front_card} style={styles.cardImage}  />
             </View>

             {/* Back Side */}
             <View style={styles.back}>
                 <Image source={this.state.array2[45].url} style={styles.cardImage} />
             </View>
      </FlipCard>
         </View>
         <View style={styles.showbtnview}>
      <TouchableOpacity onPress={()=>{this.setState({flip47: !this.state.flip47})}}  >
                   <Image source={showbtn} style={styles.showbtnImage} />
      </TouchableOpacity>
      <TouchableOpacity onPress={this.shareImage.bind(this,this.state.array2[45].base64)}>
                      <Image source={sharebtn} style={styles.showbtnImage} />
      </TouchableOpacity>
         </View>
      </View>
      {/* Card 48 */}
      <View style={styles.slide}>
         <View style={styles.cardflip}>
            <FlipCard flip={this.state.flip48} style={styles.card} friction={6} perspective={1000} flipHorizontal={true} flipVertical={false} clickable={true} alignHeight={true} onFlipEnd={(isFlipEnd)=>{console.log('isFlipEnd', isFlipEnd)}}>

             {/* Face Side */}
             <View style={styles.face}>
                 <Image source={front_card} style={styles.cardImage}  />
             </View>

             {/* Back Side */}
             <View style={styles.back}>
                 <Image source={this.state.array2[46].url} style={styles.cardImage} />
             </View>
      </FlipCard>
         </View>
         <View style={styles.showbtnview}>
      <TouchableOpacity onPress={()=>{this.setState({flip48: !this.state.flip48})}}  >
                   <Image source={showbtn} style={styles.showbtnImage} />
      </TouchableOpacity>
      <TouchableOpacity onPress={this.shareImage.bind(this,this.state.array2[46].base64)}>
                      <Image source={sharebtn} style={styles.showbtnImage} />
      </TouchableOpacity>
         </View>
      </View>
      {/* Card 49 */}
      <View style={styles.slide}>
         <View style={styles.cardflip}>
            <FlipCard flip={this.state.flip49} style={styles.card} friction={6} perspective={1000} flipHorizontal={true} flipVertical={false} clickable={true} alignHeight={true} onFlipEnd={(isFlipEnd)=>{console.log('isFlipEnd', isFlipEnd)}}>

             {/* Face Side */}
             <View style={styles.face}>
                 <Image source={front_card} style={styles.cardImage}  />
             </View>

             {/* Back Side */}
             <View style={styles.back}>
                 <Image source={this.state.array2[47].url} style={styles.cardImage} />
             </View>
      </FlipCard>
         </View>
         <View style={styles.showbtnview}>
      <TouchableOpacity onPress={()=>{this.setState({flip49: !this.state.flip49})}}  >
                   <Image source={showbtn} style={styles.showbtnImage} />
      </TouchableOpacity>
      <TouchableOpacity onPress={this.shareImage.bind(this,this.state.array2[47].base64)}>
                      <Image source={sharebtn} style={styles.showbtnImage} />
      </TouchableOpacity>
         </View>
      </View>
      {/* Card 50 */}
      <View style={styles.slide}>
         <View style={styles.cardflip}>
            <FlipCard flip={this.state.flip50} style={styles.card} friction={6} perspective={1000} flipHorizontal={true} flipVertical={false} clickable={true} alignHeight={true} onFlipEnd={(isFlipEnd)=>{console.log('isFlipEnd', isFlipEnd)}}>

             {/* Face Side */}
             <View style={styles.face}>
                 <Image source={front_card} style={styles.cardImage}  />
             </View>

             {/* Back Side */}
             <View style={styles.back}>
                 <Image source={this.state.array2[48].url} style={styles.cardImage} />
             </View>
      </FlipCard>
         </View>
         <View style={styles.showbtnview}>
      <TouchableOpacity onPress={()=>{this.setState({flip50: !this.state.flip50})}}  >
                   <Image source={showbtn} style={styles.showbtnImage} />
      </TouchableOpacity>
      <TouchableOpacity onPress={this.shareImage.bind(this,this.state.array2[48].base64)}>
                      <Image source={sharebtn} style={styles.showbtnImage} />
      </TouchableOpacity>
         </View>
      </View>

      {/* Card 51 */}
      <View style={styles.slide}>
         <View style={styles.cardflip}>
            <FlipCard flip={this.state.flip51} style={styles.card} friction={6} perspective={1000} flipHorizontal={true} flipVertical={false} clickable={true} alignHeight={true} onFlipEnd={(isFlipEnd)=>{console.log('isFlipEnd', isFlipEnd)}}>

             {/* Face Side */}
             <View style={styles.face}>
                 <Image source={front_card} style={styles.cardImage}  />
             </View>

             {/* Back Side */}
             <View style={styles.back}>
                 <Image source={this.state.array2[49].url} style={styles.cardImage} />
             </View>
      </FlipCard>
         </View>
         <View style={styles.showbtnview}>
      <TouchableOpacity onPress={()=>{this.setState({flip51: !this.state.flip51})}}  >
                   <Image source={showbtn} style={styles.showbtnImage} />
      </TouchableOpacity>
      <TouchableOpacity onPress={this.shareImage.bind(this,this.state.array2[49].base64)}>
                      <Image source={sharebtn} style={styles.showbtnImage} />
      </TouchableOpacity>
         </View>
      </View>
      {/* Card 52 */}
      <View style={styles.slide}>
         <View style={styles.cardflip}>
            <FlipCard flip={this.state.flip52} style={styles.card} friction={6} perspective={1000} flipHorizontal={true} flipVertical={false} clickable={true} alignHeight={true} onFlipEnd={(isFlipEnd)=>{console.log('isFlipEnd', isFlipEnd)}}>

             {/* Face Side */}
             <View style={styles.face}>
                 <Image source={front_card} style={styles.cardImage}  />
             </View>

             {/* Back Side */}
             <View style={styles.back}>
                 <Image source={this.state.array2[50].url} style={styles.cardImage} />
             </View>
      </FlipCard>
         </View>
         <View style={styles.showbtnview}>
      <TouchableOpacity onPress={()=>{this.setState({flip52: !this.state.flip52})}}  >
                   <Image source={showbtn} style={styles.showbtnImage} />
      </TouchableOpacity>
      <TouchableOpacity onPress={this.shareImage.bind(this,this.state.array2[50].base64)}>
                      <Image source={sharebtn} style={styles.showbtnImage} />
      </TouchableOpacity>
         </View>
      </View>
      {/* Card 53 */}
      <View style={styles.slide}>
         <View style={styles.cardflip}>
            <FlipCard flip={this.state.flip53} style={styles.card} friction={6} perspective={1000} flipHorizontal={true} flipVertical={false} clickable={true} alignHeight={true} onFlipEnd={(isFlipEnd)=>{console.log('isFlipEnd', isFlipEnd)}}>

             {/* Face Side */}
             <View style={styles.face}>
                 <Image source={front_card} style={styles.cardImage}  />
             </View>

             {/* Back Side */}
             <View style={styles.back}>
                 <Image source={this.state.array2[51].url} style={styles.cardImage} />
             </View>
      </FlipCard>
         </View>
         <View style={styles.showbtnview}>
      <TouchableOpacity onPress={()=>{this.setState({flip53: !this.state.flip53})}}  >
                   <Image source={showbtn} style={styles.showbtnImage} />
      </TouchableOpacity>
      <TouchableOpacity onPress={this.shareImage.bind(this,this.state.array2[51].base64)}>
                      <Image source={sharebtn} style={styles.showbtnImage} />
      </TouchableOpacity>
         </View>
      </View>
      {/* Card 54 */}
      <View style={styles.slide}>
         <View style={styles.cardflip}>
            <FlipCard flip={this.state.flip54} style={styles.card} friction={6} perspective={1000} flipHorizontal={true} flipVertical={false} clickable={true} alignHeight={true} onFlipEnd={(isFlipEnd)=>{console.log('isFlipEnd', isFlipEnd)}}>

             {/* Face Side */}
             <View style={styles.face}>
                 <Image source={front_card} style={styles.cardImage}  />
             </View>

             {/* Back Side */}
             <View style={styles.back}>
                 <Image source={this.state.array2[52].url} style={styles.cardImage} />
             </View>
      </FlipCard>
         </View>
         <View style={styles.showbtnview}>
      <TouchableOpacity onPress={()=>{this.setState({flip54: !this.state.flip54})}}  >
                   <Image source={showbtn} style={styles.showbtnImage} />
      </TouchableOpacity>
      <TouchableOpacity onPress={this.shareImage.bind(this,this.state.array2[52].base64)}>
                      <Image source={sharebtn} style={styles.showbtnImage} />
      </TouchableOpacity>
         </View>
      </View>
      {/* Card 55 */}
      <View style={styles.slide}>
         <View style={styles.cardflip}>
            <FlipCard flip={this.state.flip55} style={styles.card} friction={6} perspective={1000} flipHorizontal={true} flipVertical={false} clickable={true} alignHeight={true} onFlipEnd={(isFlipEnd)=>{console.log('isFlipEnd', isFlipEnd)}}>

             {/* Face Side */}
             <View style={styles.face}>
                 <Image source={front_card} style={styles.cardImage}  />
             </View>

             {/* Back Side */}
             <View style={styles.back}>
                 <Image source={this.state.array2[53].url} style={styles.cardImage} />
             </View>
      </FlipCard>
         </View>
         <View style={styles.showbtnview}>
      <TouchableOpacity onPress={()=>{this.setState({flip55: !this.state.flip55})}}  >
                   <Image source={showbtn} style={styles.showbtnImage} />
      </TouchableOpacity>
      <TouchableOpacity onPress={this.shareImage.bind(this,this.state.array2[53].base64)}>
                      <Image source={sharebtn} style={styles.showbtnImage} />
      </TouchableOpacity>
         </View>
      </View>

            </Swiper>
        </View>


    </LinearGradient>
      </View>
    </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#3bafca',
  },
  subcontainer: {
    justifyContent: 'center',
    alignItems: 'center',
    /*backgroundColor: '#3bafca',*/
  },

  card:{
    borderWidth: 0,
  },
  slide: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },

  showbtnText:{
      color:'#98bbcd',
      textAlign:'center',
  },
buttonText:{
  color:'#fff',
  justifyContent:'center',
  lineHeight:13,
  fontSize:13,
},
buttonWrapperStyle :{
  backgroundColor: 'transparent',
  flexDirection: 'row',
  position: 'absolute',
  top: '-8%',
  left: 0,
  paddingHorizontal: 5,
  paddingVertical: 5,
  justifyContent: 'space-between',
  alignItems: 'center'
},
swiperbuttonText:{
  color:'#fff',
  fontSize:40,
  fontWeight:'100',
  textShadowColor: 'rgba(0, 0, 0, 0.75)',
  textShadowOffset: {width: -1, height: 1},
  textShadowRadius: 10,

},

showbtnview:{
	flex:.2,
  marginBottom:5,
  marginTop:20,
  height:100,
  flexDirection:'row'
},
showbtnImage :{
resizeMode:'contain', height: 120, width: 120
},
cardImage :{
 resizeMode:'contain',
 width :AppSizes.screen.width-40,
 height: Platform.OS === 'android' ? AppSizes.screen.height-210 : AppSizes.screen.height-150,
},
cardflip:{
  flex:.85,
}
});
