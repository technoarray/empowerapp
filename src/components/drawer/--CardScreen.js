import React, { Component } from 'react';
import {StatusBar,Platform,StyleSheet,Text,View,Image,TouchableOpacity} from 'react-native';
import Swiper from 'react-native-swiper';
import FlipCard from 'react-native-flip-card';
import { AppStyles, AppSizes, AppColors } from '../../themes/'
import LinearGradient from 'react-native-linear-gradient';
import { NavigationActions } from 'react-navigation'
import {jsonImages} from '../data/images';


/* Images */
var logo = require( '../../themes/Images/logo.png')
var menu_icon=require( '../../themes/Images/menu_icon_144.png')
var showbtn = require( '../../themes/Images/showbtn_300.png')

//var front_card = require( '../../themes/Images/cards/front.png')
if(Platform.OS === 'ios') {
     var front_card = require( '../../themes/Images/cards/ios/front.png')
     var back_card2 = require( '../../themes/Images/cards/ios/2.png')
     var back_card3 = require( '../../themes/Images/cards/ios/3.png')
     var back_card4 = require( '../../themes/Images/cards/ios/4.png')
     var back_card5 = require( '../../themes/Images/cards/ios/5.png')
     var back_card6 = require( '../../themes/Images/cards/ios/6.png')
     var back_card7 = require( '../../themes/Images/cards/ios/7.png')
     var back_card8 = require( '../../themes/Images/cards/ios/8.png')
     var back_card9 = require( '../../themes/Images/cards/ios/9.png')
     var back_card10 = require( '../../themes/Images/cards/ios/10.png')
     var back_card11 = require( '../../themes/Images/cards/ios/11.png')
     var back_card12 = require( '../../themes/Images/cards/ios/12.png')
     var back_card13 = require( '../../themes/Images/cards/ios/13.png')
     var back_card14 = require( '../../themes/Images/cards/ios/14.png')
     var back_card15 = require( '../../themes/Images/cards/ios/15.png')
     var back_card16 = require( '../../themes/Images/cards/ios/16.png')
     var back_card17 = require( '../../themes/Images/cards/ios/17.png')
     var back_card18 = require( '../../themes/Images/cards/ios/18.png')
     var back_card19 = require( '../../themes/Images/cards/ios/19.png')
     var back_card20 = require( '../../themes/Images/cards/ios/20.png')
     var back_card21 = require( '../../themes/Images/cards/ios/21.png')
     var back_card22 = require( '../../themes/Images/cards/ios/22.png')
     var back_card23 = require( '../../themes/Images/cards/ios/23.png')
     var back_card24 = require( '../../themes/Images/cards/ios/24.png')
     var back_card25 = require( '../../themes/Images/cards/ios/25.png')
     var back_card26 = require( '../../themes/Images/cards/ios/26.png')
     var back_card27 = require( '../../themes/Images/cards/ios/27.png')
     var back_card28 = require( '../../themes/Images/cards/ios/28.png')
     var back_card29 = require( '../../themes/Images/cards/ios/29.png')
     var back_card30 = require( '../../themes/Images/cards/ios/30.png')
     var back_card31 = require( '../../themes/Images/cards/ios/31.png')
     var back_card32 = require( '../../themes/Images/cards/ios/32.png')
     var back_card33 = require( '../../themes/Images/cards/ios/33.png')
     var back_card34 = require( '../../themes/Images/cards/ios/34.png')
     var back_card35 = require( '../../themes/Images/cards/ios/35.png')
     var back_card36 = require( '../../themes/Images/cards/ios/36.png')
     var back_card37 = require( '../../themes/Images/cards/ios/37.png')
     var back_card38 = require( '../../themes/Images/cards/ios/38.png')
     var back_card39 = require( '../../themes/Images/cards/ios/39.png')
     var back_card40 = require( '../../themes/Images/cards/ios/40.png')
     var back_card41 = require( '../../themes/Images/cards/ios/41.png')
     var back_card42 = require( '../../themes/Images/cards/ios/42.png')
     var back_card43 = require( '../../themes/Images/cards/ios/43.png')
     var back_card44 = require( '../../themes/Images/cards/ios/44.png')
     var back_card45 = require( '../../themes/Images/cards/ios/45.png')
     var back_card46 = require( '../../themes/Images/cards/ios/46.png')
     var back_card47 = require( '../../themes/Images/cards/ios/47.png')
     var back_card48 = require( '../../themes/Images/cards/ios/48.png')
     var back_card49 = require( '../../themes/Images/cards/ios/49.png')
     var back_card50 = require( '../../themes/Images/cards/ios/50.png')
     var back_card51 = require( '../../themes/Images/cards/ios/51.png')
     var back_card52 = require( '../../themes/Images/cards/ios/52.png')
     var back_card53 = require( '../../themes/Images/cards/ios/53.png')
     var back_card54 = require( '../../themes/Images/cards/ios/54.png')
     var back_card55 = require( '../../themes/Images/cards/ios/55.png')
} else {
    var front_card = require( '../../themes/Images/cards/android/front.png')
    var back_card2 = require( '../../themes/Images/cards/android/2.png')
    var back_card3 = require( '../../themes/Images/cards/android/3.png')
    var back_card4 = require( '../../themes/Images/cards/android/4.png')
    var back_card5 = require( '../../themes/Images/cards/android/5.png')
    var back_card6 = require( '../../themes/Images/cards/android/6.png')
    var back_card7 = require( '../../themes/Images/cards/android/7.png')
    var back_card8 = require( '../../themes/Images/cards/android/8.png')
    var back_card9 = require( '../../themes/Images/cards/android/9.png')
    var back_card10 = require( '../../themes/Images/cards/android/10.png')
    var back_card11 = require( '../../themes/Images/cards/android/11.png')
    var back_card12 = require( '../../themes/Images/cards/android/12.png')
    var back_card13 = require( '../../themes/Images/cards/android/13.png')
    var back_card14 = require( '../../themes/Images/cards/android/14.png')
    var back_card15 = require( '../../themes/Images/cards/android/15.png')
    var back_card16 = require( '../../themes/Images/cards/android/16.png')
    var back_card17 = require( '../../themes/Images/cards/android/17.png')
    var back_card18 = require( '../../themes/Images/cards/android/18.png')
    var back_card19 = require( '../../themes/Images/cards/android/19.png')
    var back_card20 = require( '../../themes/Images/cards/android/20.png')
    var back_card21 = require( '../../themes/Images/cards/android/21.png')
    var back_card22 = require( '../../themes/Images/cards/android/22.png')
    var back_card23 = require( '../../themes/Images/cards/android/23.png')
    var back_card24 = require( '../../themes/Images/cards/android/24.png')
    var back_card25 = require( '../../themes/Images/cards/android/25.png')
    var back_card26 = require( '../../themes/Images/cards/android/26.png')
    var back_card27 = require( '../../themes/Images/cards/android/27.png')
    var back_card28 = require( '../../themes/Images/cards/android/28.png')
    var back_card29 = require( '../../themes/Images/cards/android/29.png')
    var back_card30 = require( '../../themes/Images/cards/android/30.png')
    var back_card31 = require( '../../themes/Images/cards/android/31.png')
    var back_card32 = require( '../../themes/Images/cards/android/32.png')
    var back_card33 = require( '../../themes/Images/cards/android/33.png')
    var back_card34 = require( '../../themes/Images/cards/android/34.png')
    var back_card35 = require( '../../themes/Images/cards/android/35.png')
    var back_card36 = require( '../../themes/Images/cards/android/36.png')
    var back_card37 = require( '../../themes/Images/cards/android/37.png')
    var back_card38 = require( '../../themes/Images/cards/android/38.png')
    var back_card39 = require( '../../themes/Images/cards/android/39.png')
    var back_card40 = require( '../../themes/Images/cards/android/40.png')
    var back_card41 = require( '../../themes/Images/cards/android/41.png')
    var back_card42 = require( '../../themes/Images/cards/android/42.png')
    var back_card43 = require( '../../themes/Images/cards/android/43.png')
    var back_card44 = require( '../../themes/Images/cards/android/44.png')
    var back_card45 = require( '../../themes/Images/cards/android/45.png')
    var back_card46 = require( '../../themes/Images/cards/android/46.png')
    var back_card47 = require( '../../themes/Images/cards/android/47.png')
    var back_card48 = require( '../../themes/Images/cards/android/48.png')
    var back_card49 = require( '../../themes/Images/cards/android/49.png')
    var back_card50 = require( '../../themes/Images/cards/android/50.png')
    var back_card51 = require( '../../themes/Images/cards/android/51.png')
    var back_card52 = require( '../../themes/Images/cards/android/52.png')
    var back_card53 = require( '../../themes/Images/cards/android/53.png')
    var back_card54 = require( '../../themes/Images/cards/android/54.png')
    var back_card55 = require( '../../themes/Images/cards/android/55.png')
}



let _that

export default class CardScreen extends Component {
  constructor (props) {
    super(props)
    this.state = {
      flip2: false,
      flip3: false,
      flip4: false,
      items: [],
      isVisible: false,
    },
      _that = this;
  }

  componentWillMount(){
    let images_arr=[];
    console.log("imagedata",jsonImages);
    for(var i in jsonImages){
      //images_arr.push([i, jsonImages [i]]);
      images_arr[i]=jsonImages[i];
    }


    console.log(images_arr)
    let array2=[];
    while(images_arr.length !== 0){
      let randomIndex=Math.floor(Math.random() * images_arr.length);
      array2.push(images_arr[randomIndex]);
      images_arr.splice(randomIndex,1);
    }
    console.log("imagedata",array2);
  }
    menu_click() {
    //alert('hello');
      _that.props.navigation.openDrawer();
  }

  render() {
    return (
      <View style={styles.container}>
      <StatusBar hidden={true} />
      <View style={styles.subcontainer}>
      <LinearGradient colors={['#3baeca', '#3baeca', '#3983ab', '#386b98']}>
      <View style={{ width:AppSizes.screen.width,flex:.07,flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', marginTop: 20,marginBottom: 20}}>
          <View style={{ marginTop:0, paddingLeft:10}}>
              <TouchableOpacity onPress={this.menu_click}>
                  <Image source={menu_icon} style={{resizeMode: 'contain', width: 30, height: 30, }} />
              </TouchableOpacity>
          </View>
          <View style={{ marginTop:0}}>
              <Image source={logo} style={{resizeMode: 'contain', height: 200, width: 200}} />
          </View>
          <View style={{ marginTop:0}}>

          </View>
      </View>

      <View style={{flex:.95,width:AppSizes.screen.width}}>
          <Swiper style={styles.wrapper} showsButtons={true} showsPagination={false}
          buttonWrapperStyle={styles.buttonWrapperStyle}
          nextButton={<Text style={styles.swiperbuttonText}>›</Text>}
          prevButton={<Text style={styles.swiperbuttonText}>‹</Text>}
          >


            {/* Card 2 */}
             <View style={styles.slide}>
                <View style={styles.cardflip}>
                   <FlipCard flip={this.state.flip2} style={styles.card} friction={6} perspective={1000} flipHorizontal={true} flipVertical={false} clickable={true} alignHeight={true} onFlipEnd={(isFlipEnd)=>{console.log('isFlipEnd', isFlipEnd)}}>

                    {/* Face Side */}
                    <View style={styles.face}>
                        <Image source={front_card} style={styles.cardImage}  />
                    </View>

                    {/* Back Side */}
                    <View style={styles.back}>
                        <Image source={back_card2} style={styles.cardImage} />
                    </View>
					</FlipCard>
                </View>
                <View style={styles.showbtnview}>
					<TouchableOpacity onPress={()=>{this.setState({flip2: !this.state.flip2})}}  >
                          <Image source={showbtn} style={styles.showbtnImage} />
					</TouchableOpacity>
                </View>
            </View>

			{/* Card 3 */}
             <View style={styles.slide}>
                <View style={styles.cardflip}>
                   <FlipCard flip={this.state.flip3} style={styles.card} friction={6} perspective={1000} flipHorizontal={true} flipVertical={false} clickable={true} alignHeight={true} onFlipEnd={(isFlipEnd)=>{console.log('isFlipEnd', isFlipEnd)}}>

                    {/* Face Side */}
                    <View style={styles.face}>
                        <Image source={front_card} style={styles.cardImage}  />
                    </View>

                    {/* Back Side */}
                    <View style={styles.back}>
                        <Image source={back_card3} style={styles.cardImage} />
                    </View>
					</FlipCard>
                </View>
                <View style={styles.showbtnview}>
					<TouchableOpacity onPress={()=>{this.setState({flip3: !this.state.flip3})}}  >
                <Image source={showbtn} style={styles.showbtnImage} />
					</TouchableOpacity>
                </View>
            </View>

			{/* Card 4 */}
             <View style={styles.slide}>
                <View style={styles.cardflip}>
                   <FlipCard flip={this.state.flip4} style={styles.card} friction={6} perspective={1000} flipHorizontal={true} flipVertical={false} clickable={true} alignHeight={true} onFlipEnd={(isFlipEnd)=>{console.log('isFlipEnd', isFlipEnd)}}>

                    {/* Face Side */}
                    <View style={styles.face}>
                        <Image source={front_card} style={styles.cardImage}  />
                    </View>

                    {/* Back Side */}
                    <View style={styles.back}>
                        <Image source={back_card4} style={styles.cardImage} />
                    </View>
					</FlipCard>
                </View>
                <View style={styles.showbtnview}>
					<TouchableOpacity onPress={()=>{this.setState({flip4: !this.state.flip4})}}  >
                          <Image source={showbtn} style={styles.showbtnImage} />
					</TouchableOpacity>
                </View>
            </View>

			{/* Card 5 */}
             <View style={styles.slide}>
                <View style={styles.cardflip}>
                   <FlipCard flip={this.state.flip5} style={styles.card} friction={6} perspective={1000} flipHorizontal={true} flipVertical={false} clickable={true} alignHeight={true} onFlipEnd={(isFlipEnd)=>{console.log('isFlipEnd', isFlipEnd)}}>

                    {/* Face Side */}
                    <View style={styles.face}>
                        <Image source={front_card} style={styles.cardImage}  />
                    </View>

                    {/* Back Side */}
                    <View style={styles.back}>
                        <Image source={back_card5} style={styles.cardImage} />
                    </View>
					</FlipCard>
                </View>
                <View style={styles.showbtnview}>
					<TouchableOpacity onPress={()=>{this.setState({flip5: !this.state.flip5})}}  >
                          <Image source={showbtn} style={styles.showbtnImage} />
					</TouchableOpacity>
                </View>
            </View>

			{/* Card 6 */}
             <View style={styles.slide}>
                <View style={styles.cardflip}>
                   <FlipCard flip={this.state.flip6} style={styles.card} friction={6} perspective={1000} flipHorizontal={true} flipVertical={false} clickable={true} alignHeight={true} onFlipEnd={(isFlipEnd)=>{console.log('isFlipEnd', isFlipEnd)}}>

                    {/* Face Side */}
                    <View style={styles.face}>
                        <Image source={front_card} style={styles.cardImage}  />
                    </View>

                    {/* Back Side */}
                    <View style={styles.back}>
                        <Image source={back_card6} style={styles.cardImage} />
                    </View>
					</FlipCard>
                </View>
                <View style={styles.showbtnview}>
					<TouchableOpacity onPress={()=>{this.setState({flip6: !this.state.flip6})}}  >
                          <Image source={showbtn} style={styles.showbtnImage} />
					</TouchableOpacity>
                </View>
            </View>

			{/* Card 7 */}
             <View style={styles.slide}>
                <View style={styles.cardflip}>
                   <FlipCard flip={this.state.flip7} style={styles.card} friction={6} perspective={1000} flipHorizontal={true} flipVertical={false} clickable={true} alignHeight={true} onFlipEnd={(isFlipEnd)=>{console.log('isFlipEnd', isFlipEnd)}}>

                    {/* Face Side */}
                    <View style={styles.face}>
                        <Image source={front_card} style={styles.cardImage}  />
                    </View>

                    {/* Back Side */}
                    <View style={styles.back}>
                        <Image source={back_card7} style={styles.cardImage} />
                    </View>
					</FlipCard>
                </View>
                <View style={styles.showbtnview}>
					<TouchableOpacity onPress={()=>{this.setState({flip7: !this.state.flip7})}}  >
                          <Image source={showbtn} style={styles.showbtnImage} />
					</TouchableOpacity>
                </View>
            </View>

			{/* Card 8 */}
             <View style={styles.slide}>
                <View style={styles.cardflip}>
                   <FlipCard flip={this.state.flip8} style={styles.card} friction={6} perspective={1000} flipHorizontal={true} flipVertical={false} clickable={true} alignHeight={true} onFlipEnd={(isFlipEnd)=>{console.log('isFlipEnd', isFlipEnd)}}>

                    {/* Face Side */}
                    <View style={styles.face}>
                        <Image source={front_card} style={styles.cardImage}  />
                    </View>

                    {/* Back Side */}
                    <View style={styles.back}>
                        <Image source={back_card8} style={styles.cardImage} />
                    </View>
					</FlipCard>
                </View>
                <View style={styles.showbtnview}>
					<TouchableOpacity onPress={()=>{this.setState({flip8: !this.state.flip8})}}  >
                          <Image source={showbtn} style={styles.showbtnImage} />
					</TouchableOpacity>
                </View>
            </View>

			{/* Card 9 */}
             <View style={styles.slide}>
                <View style={styles.cardflip}>
                   <FlipCard flip={this.state.flip9} style={styles.card} friction={6} perspective={1000} flipHorizontal={true} flipVertical={false} clickable={true} alignHeight={true} onFlipEnd={(isFlipEnd)=>{console.log('isFlipEnd', isFlipEnd)}}>

                    {/* Face Side */}
                    <View style={styles.face}>
                        <Image source={front_card} style={styles.cardImage}  />
                    </View>

                    {/* Back Side */}
                    <View style={styles.back}>
                        <Image source={back_card9} style={styles.cardImage} />
                    </View>
					</FlipCard>
                </View>
                <View style={styles.showbtnview}>
					<TouchableOpacity onPress={()=>{this.setState({flip9: !this.state.flip9})}}  >
                          <Image source={showbtn} style={styles.showbtnImage} />
					</TouchableOpacity>
                </View>
            </View>

			{/* Card 10 */}
             <View style={styles.slide}>
                <View style={styles.cardflip}>
                   <FlipCard flip={this.state.flip10} style={styles.card} friction={6} perspective={1000} flipHorizontal={true} flipVertical={false} clickable={true} alignHeight={true} onFlipEnd={(isFlipEnd)=>{console.log('isFlipEnd', isFlipEnd)}}>

                    {/* Face Side */}
                    <View style={styles.face}>
                        <Image source={front_card} style={styles.cardImage}  />
                    </View>

                    {/* Back Side */}
                    <View style={styles.back}>
                        <Image source={back_card10} style={styles.cardImage} />
                    </View>
					</FlipCard>
                </View>
                <View style={styles.showbtnview}>
					<TouchableOpacity onPress={()=>{this.setState({flip10: !this.state.flip10})}}  >
                          <Image source={showbtn} style={styles.showbtnImage} />
					</TouchableOpacity>
                </View>
            </View>

			{/* Card 11 */}
             <View style={styles.slide}>
                <View style={styles.cardflip}>
                   <FlipCard flip={this.state.flip11} style={styles.card} friction={6} perspective={1000} flipHorizontal={true} flipVertical={false} clickable={true} alignHeight={true} onFlipEnd={(isFlipEnd)=>{console.log('isFlipEnd', isFlipEnd)}}>

                    {/* Face Side */}
                    <View style={styles.face}>
                        <Image source={front_card} style={styles.cardImage}  />
                    </View>

                    {/* Back Side */}
                    <View style={styles.back}>
                        <Image source={back_card11} style={styles.cardImage} />
                    </View>
					</FlipCard>
                </View>
                <View style={styles.showbtnview}>
					<TouchableOpacity onPress={()=>{this.setState({flip11: !this.state.flip11})}}  >
                          <Image source={showbtn} style={styles.showbtnImage} />
					</TouchableOpacity>
                </View>
            </View>

			{/* Card 12 */}
             <View style={styles.slide}>
                <View style={styles.cardflip}>
                   <FlipCard flip={this.state.flip12} style={styles.card} friction={6} perspective={1000} flipHorizontal={true} flipVertical={false} clickable={true} alignHeight={true} onFlipEnd={(isFlipEnd)=>{console.log('isFlipEnd', isFlipEnd)}}>

                    {/* Face Side */}
                    <View style={styles.face}>
                        <Image source={front_card} style={styles.cardImage}  />
                    </View>

                    {/* Back Side */}
                    <View style={styles.back}>
                        <Image source={back_card12} style={styles.cardImage} />
                    </View>
					</FlipCard>
                </View>
                <View style={styles.showbtnview}>
					<TouchableOpacity onPress={()=>{this.setState({flip12: !this.state.flip12})}}  >
                          <Image source={showbtn} style={styles.showbtnImage} />
					</TouchableOpacity>
                </View>
            </View>

			{/* Card 13 */}
             <View style={styles.slide}>
                <View style={styles.cardflip}>
                   <FlipCard flip={this.state.flip13} style={styles.card} friction={6} perspective={1000} flipHorizontal={true} flipVertical={false} clickable={true} alignHeight={true} onFlipEnd={(isFlipEnd)=>{console.log('isFlipEnd', isFlipEnd)}}>

                    {/* Face Side */}
                    <View style={styles.face}>
                        <Image source={front_card} style={styles.cardImage}  />
                    </View>

                    {/* Back Side */}
                    <View style={styles.back}>
                        <Image source={back_card13} style={styles.cardImage} />
                    </View>
					</FlipCard>
                </View>
                <View style={styles.showbtnview}>
					<TouchableOpacity onPress={()=>{this.setState({flip13: !this.state.flip13})}}  >
                          <Image source={showbtn} style={styles.showbtnImage} />
					</TouchableOpacity>
                </View>
            </View>

			{/* Card 14 */}
             <View style={styles.slide}>
                <View style={styles.cardflip}>
                   <FlipCard flip={this.state.flip14} style={styles.card} friction={6} perspective={1000} flipHorizontal={true} flipVertical={false} clickable={true} alignHeight={true} onFlipEnd={(isFlipEnd)=>{console.log('isFlipEnd', isFlipEnd)}}>

                    {/* Face Side */}
                    <View style={styles.face}>
                        <Image source={front_card} style={styles.cardImage}  />
                    </View>

                    {/* Back Side */}
                    <View style={styles.back}>
                        <Image source={back_card14} style={styles.cardImage} />
                    </View>
					</FlipCard>
                </View>
                <View style={styles.showbtnview}>
					<TouchableOpacity onPress={()=>{this.setState({flip14: !this.state.flip14})}}  >
                          <Image source={showbtn} style={styles.showbtnImage} />
					</TouchableOpacity>
                </View>
            </View>

			{/* Card 15 */}
             <View style={styles.slide}>
                <View style={styles.cardflip}>
                   <FlipCard flip={this.state.flip15} style={styles.card} friction={6} perspective={1000} flipHorizontal={true} flipVertical={false} clickable={true} alignHeight={true} onFlipEnd={(isFlipEnd)=>{console.log('isFlipEnd', isFlipEnd)}}>

                    {/* Face Side */}
                    <View style={styles.face}>
                        <Image source={front_card} style={styles.cardImage}  />
                    </View>

                    {/* Back Side */}
                    <View style={styles.back}>
                        <Image source={back_card15} style={styles.cardImage} />
                    </View>
					</FlipCard>
                </View>
                <View style={styles.showbtnview}>
					<TouchableOpacity onPress={()=>{this.setState({flip15: !this.state.flip15})}}  >
                          <Image source={showbtn} style={styles.showbtnImage} />
					</TouchableOpacity>
                </View>
            </View>
			{/* Card 16 */}
             <View style={styles.slide}>
                <View style={styles.cardflip}>
                   <FlipCard flip={this.state.flip16} style={styles.card} friction={6} perspective={1000} flipHorizontal={true} flipVertical={false} clickable={true} alignHeight={true} onFlipEnd={(isFlipEnd)=>{console.log('isFlipEnd', isFlipEnd)}}>

                    {/* Face Side */}
                    <View style={styles.face}>
                        <Image source={front_card} style={styles.cardImage}  />
                    </View>

                    {/* Back Side */}
                    <View style={styles.back}>
                        <Image source={back_card16} style={styles.cardImage} />
                    </View>
					</FlipCard>
                </View>
                <View style={styles.showbtnview}>
					<TouchableOpacity onPress={()=>{this.setState({flip16: !this.state.flip16})}}  >
                          <Image source={showbtn} style={styles.showbtnImage} />
					</TouchableOpacity>
                </View>
            </View>
			{/* Card 17 */}
             <View style={styles.slide}>
                <View style={styles.cardflip}>
                   <FlipCard flip={this.state.flip17} style={styles.card} friction={6} perspective={1000} flipHorizontal={true} flipVertical={false} clickable={true} alignHeight={true} onFlipEnd={(isFlipEnd)=>{console.log('isFlipEnd', isFlipEnd)}}>

                    {/* Face Side */}
                    <View style={styles.face}>
                        <Image source={front_card} style={styles.cardImage}  />
                    </View>

                    {/* Back Side */}
                    <View style={styles.back}>
                        <Image source={back_card17} style={styles.cardImage} />
                    </View>
					</FlipCard>
                </View>
                <View style={styles.showbtnview}>
					<TouchableOpacity onPress={()=>{this.setState({flip17: !this.state.flip17})}}  >
                          <Image source={showbtn} style={styles.showbtnImage} />
					</TouchableOpacity>
                </View>
            </View>
			{/* Card 18 */}
             <View style={styles.slide}>
                <View style={styles.cardflip}>
                   <FlipCard flip={this.state.flip18} style={styles.card} friction={6} perspective={1000} flipHorizontal={true} flipVertical={false} clickable={true} alignHeight={true} onFlipEnd={(isFlipEnd)=>{console.log('isFlipEnd', isFlipEnd)}}>

                    {/* Face Side */}
                    <View style={styles.face}>
                        <Image source={front_card} style={styles.cardImage}  />
                    </View>

                    {/* Back Side */}
                    <View style={styles.back}>
                        <Image source={back_card18} style={styles.cardImage} />
                    </View>
					</FlipCard>
                </View>
                <View style={styles.showbtnview}>
					<TouchableOpacity onPress={()=>{this.setState({flip18: !this.state.flip18})}}  >
                          <Image source={showbtn} style={styles.showbtnImage} />
					</TouchableOpacity>
                </View>
            </View>
			{/* Card 19 */}
             <View style={styles.slide}>
                <View style={styles.cardflip}>
                   <FlipCard flip={this.state.flip19} style={styles.card} friction={6} perspective={1000} flipHorizontal={true} flipVertical={false} clickable={true} alignHeight={true} onFlipEnd={(isFlipEnd)=>{console.log('isFlipEnd', isFlipEnd)}}>

                    {/* Face Side */}
                    <View style={styles.face}>
                        <Image source={front_card} style={styles.cardImage}  />
                    </View>

                    {/* Back Side */}
                    <View style={styles.back}>
                        <Image source={back_card19} style={styles.cardImage} />
                    </View>
					</FlipCard>
                </View>
                <View style={styles.showbtnview}>
					<TouchableOpacity onPress={()=>{this.setState({flip19: !this.state.flip19})}}  >
                          <Image source={showbtn} style={styles.showbtnImage} />
					</TouchableOpacity>
                </View>
            </View>
			{/* Card 20 */}
      <View style={styles.slide}>
         <View style={styles.cardflip}>
            <FlipCard flip={this.state.flip20} style={styles.card} friction={6} perspective={1000} flipHorizontal={true} flipVertical={false} clickable={true} alignHeight={true} onFlipEnd={(isFlipEnd)=>{console.log('isFlipEnd', isFlipEnd)}}>

             {/* Face Side */}
             <View style={styles.face}>
                 <Image source={front_card} style={styles.cardImage}  />
             </View>

             {/* Back Side */}
             <View style={styles.back}>
                 <Image source={back_card20} style={styles.cardImage} />
             </View>
      </FlipCard>
         </View>
         <View style={styles.showbtnview}>
      <TouchableOpacity onPress={()=>{this.setState({flip20: !this.state.flip20})}}  >
                   <Image source={showbtn} style={styles.showbtnImage} />
      </TouchableOpacity>
         </View>
      </View>
      {/* Card 21 */}
      <View style={styles.slide}>
         <View style={styles.cardflip}>
            <FlipCard flip={this.state.flip21} style={styles.card} friction={6} perspective={1000} flipHorizontal={true} flipVertical={false} clickable={true} alignHeight={true} onFlipEnd={(isFlipEnd)=>{console.log('isFlipEnd', isFlipEnd)}}>

             {/* Face Side */}
             <View style={styles.face}>
                 <Image source={front_card} style={styles.cardImage}  />
             </View>

             {/* Back Side */}
             <View style={styles.back}>
                 <Image source={back_card21} style={styles.cardImage} />
             </View>
      </FlipCard>
         </View>
         <View style={styles.showbtnview}>
      <TouchableOpacity onPress={()=>{this.setState({flip21: !this.state.flip21})}}  >
                   <Image source={showbtn} style={styles.showbtnImage} />
      </TouchableOpacity>
         </View>
      </View>
      {/* Card 22 */}
      <View style={styles.slide}>
         <View style={styles.cardflip}>
            <FlipCard flip={this.state.flip22} style={styles.card} friction={6} perspective={1000} flipHorizontal={true} flipVertical={false} clickable={true} alignHeight={true} onFlipEnd={(isFlipEnd)=>{console.log('isFlipEnd', isFlipEnd)}}>

             {/* Face Side */}
             <View style={styles.face}>
                 <Image source={front_card} style={styles.cardImage}  />
             </View>

             {/* Back Side */}
             <View style={styles.back}>
                 <Image source={back_card22} style={styles.cardImage} />
             </View>
      </FlipCard>
         </View>
         <View style={styles.showbtnview}>
      <TouchableOpacity onPress={()=>{this.setState({flip22: !this.state.flip22})}}  >
                   <Image source={showbtn} style={styles.showbtnImage} />
      </TouchableOpacity>
         </View>
      </View>
      {/* Card 23 */}
      <View style={styles.slide}>
         <View style={styles.cardflip}>
            <FlipCard flip={this.state.flip23} style={styles.card} friction={6} perspective={1000} flipHorizontal={true} flipVertical={false} clickable={true} alignHeight={true} onFlipEnd={(isFlipEnd)=>{console.log('isFlipEnd', isFlipEnd)}}>

             {/* Face Side */}
             <View style={styles.face}>
                 <Image source={front_card} style={styles.cardImage}  />
             </View>

             {/* Back Side */}
             <View style={styles.back}>
                 <Image source={back_card23} style={styles.cardImage} />
             </View>
      </FlipCard>
         </View>
         <View style={styles.showbtnview}>
      <TouchableOpacity onPress={()=>{this.setState({flip23: !this.state.flip23})}}  >
                   <Image source={showbtn} style={styles.showbtnImage} />
      </TouchableOpacity>
         </View>
      </View>
      {/* Card 24 */}
      <View style={styles.slide}>
         <View style={styles.cardflip}>
            <FlipCard flip={this.state.flip24} style={styles.card} friction={6} perspective={1000} flipHorizontal={true} flipVertical={false} clickable={true} alignHeight={true} onFlipEnd={(isFlipEnd)=>{console.log('isFlipEnd', isFlipEnd)}}>

             {/* Face Side */}
             <View style={styles.face}>
                 <Image source={front_card} style={styles.cardImage}  />
             </View>

             {/* Back Side */}
             <View style={styles.back}>
                 <Image source={back_card24} style={styles.cardImage} />
             </View>
      </FlipCard>
         </View>
         <View style={styles.showbtnview}>
      <TouchableOpacity onPress={()=>{this.setState({flip24: !this.state.flip24})}}  >
                   <Image source={showbtn} style={styles.showbtnImage} />
      </TouchableOpacity>
         </View>
      </View>
      {/* Card 25 */}
      <View style={styles.slide}>
         <View style={styles.cardflip}>
            <FlipCard flip={this.state.flip25} style={styles.card} friction={6} perspective={1000} flipHorizontal={true} flipVertical={false} clickable={true} alignHeight={true} onFlipEnd={(isFlipEnd)=>{console.log('isFlipEnd', isFlipEnd)}}>

             {/* Face Side */}
             <View style={styles.face}>
                 <Image source={front_card} style={styles.cardImage}  />
             </View>

             {/* Back Side */}
             <View style={styles.back}>
                 <Image source={back_card25} style={styles.cardImage} />
             </View>
      </FlipCard>
         </View>
         <View style={styles.showbtnview}>
      <TouchableOpacity onPress={()=>{this.setState({flip25: !this.state.flip25})}}  >
                   <Image source={showbtn} style={styles.showbtnImage} />
      </TouchableOpacity>
         </View>
      </View>
      {/* Card 26 */}
      <View style={styles.slide}>
         <View style={styles.cardflip}>
            <FlipCard flip={this.state.flip26} style={styles.card} friction={6} perspective={1000} flipHorizontal={true} flipVertical={false} clickable={true} alignHeight={true} onFlipEnd={(isFlipEnd)=>{console.log('isFlipEnd', isFlipEnd)}}>

             {/* Face Side */}
             <View style={styles.face}>
                 <Image source={front_card} style={styles.cardImage}  />
             </View>

             {/* Back Side */}
             <View style={styles.back}>
                 <Image source={back_card26} style={styles.cardImage} />
             </View>
      </FlipCard>
         </View>
         <View style={styles.showbtnview}>
      <TouchableOpacity onPress={()=>{this.setState({flip26: !this.state.flip26})}}  >
                   <Image source={showbtn} style={styles.showbtnImage} />
      </TouchableOpacity>
         </View>
      </View>
      {/* Card 27 */}
      <View style={styles.slide}>
         <View style={styles.cardflip}>
            <FlipCard flip={this.state.flip27} style={styles.card} friction={6} perspective={1000} flipHorizontal={true} flipVertical={false} clickable={true} alignHeight={true} onFlipEnd={(isFlipEnd)=>{console.log('isFlipEnd', isFlipEnd)}}>

             {/* Face Side */}
             <View style={styles.face}>
                 <Image source={front_card} style={styles.cardImage}  />
             </View>

             {/* Back Side */}
             <View style={styles.back}>
                 <Image source={back_card27} style={styles.cardImage} />
             </View>
      </FlipCard>
         </View>
         <View style={styles.showbtnview}>
      <TouchableOpacity onPress={()=>{this.setState({flip27: !this.state.flip27})}}  >
                   <Image source={showbtn} style={styles.showbtnImage} />
      </TouchableOpacity>
         </View>
      </View>
      {/* Card 28 */}
      <View style={styles.slide}>
         <View style={styles.cardflip}>
            <FlipCard flip={this.state.flip28} style={styles.card} friction={6} perspective={1000} flipHorizontal={true} flipVertical={false} clickable={true} alignHeight={true} onFlipEnd={(isFlipEnd)=>{console.log('isFlipEnd', isFlipEnd)}}>

             {/* Face Side */}
             <View style={styles.face}>
                 <Image source={front_card} style={styles.cardImage}  />
             </View>

             {/* Back Side */}
             <View style={styles.back}>
                 <Image source={back_card28} style={styles.cardImage} />
             </View>
      </FlipCard>
         </View>
         <View style={styles.showbtnview}>
      <TouchableOpacity onPress={()=>{this.setState({flip28: !this.state.flip28})}}  >
                   <Image source={showbtn} style={styles.showbtnImage} />
      </TouchableOpacity>
         </View>
      </View>
      {/* Card 29 */}
      <View style={styles.slide}>
         <View style={styles.cardflip}>
            <FlipCard flip={this.state.flip29} style={styles.card} friction={6} perspective={1000} flipHorizontal={true} flipVertical={false} clickable={true} alignHeight={true} onFlipEnd={(isFlipEnd)=>{console.log('isFlipEnd', isFlipEnd)}}>

             {/* Face Side */}
             <View style={styles.face}>
                 <Image source={front_card} style={styles.cardImage}  />
             </View>

             {/* Back Side */}
             <View style={styles.back}>
                 <Image source={back_card29} style={styles.cardImage} />
             </View>
      </FlipCard>
         </View>
         <View style={styles.showbtnview}>
      <TouchableOpacity onPress={()=>{this.setState({flip29: !this.state.flip29})}}  >
                   <Image source={showbtn} style={styles.showbtnImage} />
      </TouchableOpacity>
         </View>
      </View>
      {/* Card 30 */}
      <View style={styles.slide}>
         <View style={styles.cardflip}>
            <FlipCard flip={this.state.flip30} style={styles.card} friction={6} perspective={1000} flipHorizontal={true} flipVertical={false} clickable={true} alignHeight={true} onFlipEnd={(isFlipEnd)=>{console.log('isFlipEnd', isFlipEnd)}}>

             {/* Face Side */}
             <View style={styles.face}>
                 <Image source={front_card} style={styles.cardImage}  />
             </View>

             {/* Back Side */}
             <View style={styles.back}>
                 <Image source={back_card30} style={styles.cardImage} />
             </View>
      </FlipCard>
         </View>
         <View style={styles.showbtnview}>
      <TouchableOpacity onPress={()=>{this.setState({flip30: !this.state.flip30})}}  >
                   <Image source={showbtn} style={styles.showbtnImage} />
      </TouchableOpacity>
         </View>
      </View>
      {/* Card 31 */}
      <View style={styles.slide}>
         <View style={styles.cardflip}>
            <FlipCard flip={this.state.flip31} style={styles.card} friction={6} perspective={1000} flipHorizontal={true} flipVertical={false} clickable={true} alignHeight={true} onFlipEnd={(isFlipEnd)=>{console.log('isFlipEnd', isFlipEnd)}}>

             {/* Face Side */}
             <View style={styles.face}>
                 <Image source={front_card} style={styles.cardImage}  />
             </View>

             {/* Back Side */}
             <View style={styles.back}>
                 <Image source={back_card31} style={styles.cardImage} />
             </View>
      </FlipCard>
         </View>
         <View style={styles.showbtnview}>
      <TouchableOpacity onPress={()=>{this.setState({flip31: !this.state.flip31})}}  >
                   <Image source={showbtn} style={styles.showbtnImage} />
      </TouchableOpacity>
         </View>
      </View>
      {/* Card 32 */}
      <View style={styles.slide}>
         <View style={styles.cardflip}>
            <FlipCard flip={this.state.flip32} style={styles.card} friction={6} perspective={1000} flipHorizontal={true} flipVertical={false} clickable={true} alignHeight={true} onFlipEnd={(isFlipEnd)=>{console.log('isFlipEnd', isFlipEnd)}}>

             {/* Face Side */}
             <View style={styles.face}>
                 <Image source={front_card} style={styles.cardImage}  />
             </View>

             {/* Back Side */}
             <View style={styles.back}>
                 <Image source={back_card32} style={styles.cardImage} />
             </View>
      </FlipCard>
         </View>
         <View style={styles.showbtnview}>
      <TouchableOpacity onPress={()=>{this.setState({flip32: !this.state.flip32})}}  >
                   <Image source={showbtn} style={styles.showbtnImage} />
      </TouchableOpacity>
         </View>
      </View>
      {/* Card 33 */}
      <View style={styles.slide}>
         <View style={styles.cardflip}>
            <FlipCard flip={this.state.flip33} style={styles.card} friction={6} perspective={1000} flipHorizontal={true} flipVertical={false} clickable={true} alignHeight={true} onFlipEnd={(isFlipEnd)=>{console.log('isFlipEnd', isFlipEnd)}}>

             {/* Face Side */}
             <View style={styles.face}>
                 <Image source={front_card} style={styles.cardImage}  />
             </View>

             {/* Back Side */}
             <View style={styles.back}>
                 <Image source={back_card33} style={styles.cardImage} />
             </View>
      </FlipCard>
         </View>
         <View style={styles.showbtnview}>
      <TouchableOpacity onPress={()=>{this.setState({flip33: !this.state.flip33})}}  >
                   <Image source={showbtn} style={styles.showbtnImage} />
      </TouchableOpacity>
         </View>
      </View>
      {/* Card 34 */}
      <View style={styles.slide}>
         <View style={styles.cardflip}>
            <FlipCard flip={this.state.flip34} style={styles.card} friction={6} perspective={1000} flipHorizontal={true} flipVertical={false} clickable={true} alignHeight={true} onFlipEnd={(isFlipEnd)=>{console.log('isFlipEnd', isFlipEnd)}}>

             {/* Face Side */}
             <View style={styles.face}>
                 <Image source={front_card} style={styles.cardImage}  />
             </View>

             {/* Back Side */}
             <View style={styles.back}>
                 <Image source={back_card34} style={styles.cardImage} />
             </View>
      </FlipCard>
         </View>
         <View style={styles.showbtnview}>
      <TouchableOpacity onPress={()=>{this.setState({flip34: !this.state.flip34})}}  >
                   <Image source={showbtn} style={styles.showbtnImage} />
      </TouchableOpacity>
         </View>
      </View>
      {/* Card 35 */}
      <View style={styles.slide}>
         <View style={styles.cardflip}>
            <FlipCard flip={this.state.flip35} style={styles.card} friction={6} perspective={1000} flipHorizontal={true} flipVertical={false} clickable={true} alignHeight={true} onFlipEnd={(isFlipEnd)=>{console.log('isFlipEnd', isFlipEnd)}}>

             {/* Face Side */}
             <View style={styles.face}>
                 <Image source={front_card} style={styles.cardImage}  />
             </View>

             {/* Back Side */}
             <View style={styles.back}>
                 <Image source={back_card35} style={styles.cardImage} />
             </View>
      </FlipCard>
         </View>
         <View style={styles.showbtnview}>
      <TouchableOpacity onPress={()=>{this.setState({flip35: !this.state.flip35})}}  >
                   <Image source={showbtn} style={styles.showbtnImage} />
      </TouchableOpacity>
         </View>
      </View>
      {/* Card 36 */}
      <View style={styles.slide}>
         <View style={styles.cardflip}>
            <FlipCard flip={this.state.flip36} style={styles.card} friction={6} perspective={1000} flipHorizontal={true} flipVertical={false} clickable={true} alignHeight={true} onFlipEnd={(isFlipEnd)=>{console.log('isFlipEnd', isFlipEnd)}}>

             {/* Face Side */}
             <View style={styles.face}>
                 <Image source={front_card} style={styles.cardImage}  />
             </View>

             {/* Back Side */}
             <View style={styles.back}>
                 <Image source={back_card36} style={styles.cardImage} />
             </View>
      </FlipCard>
         </View>
         <View style={styles.showbtnview}>
      <TouchableOpacity onPress={()=>{this.setState({flip36: !this.state.flip36})}}  >
                   <Image source={showbtn} style={styles.showbtnImage} />
      </TouchableOpacity>
         </View>
      </View>
      {/* Card 37 */}
      <View style={styles.slide}>
         <View style={styles.cardflip}>
            <FlipCard flip={this.state.flip37} style={styles.card} friction={6} perspective={1000} flipHorizontal={true} flipVertical={false} clickable={true} alignHeight={true} onFlipEnd={(isFlipEnd)=>{console.log('isFlipEnd', isFlipEnd)}}>

             {/* Face Side */}
             <View style={styles.face}>
                 <Image source={front_card} style={styles.cardImage}  />
             </View>

             {/* Back Side */}
             <View style={styles.back}>
                 <Image source={back_card37} style={styles.cardImage} />
             </View>
      </FlipCard>
         </View>
         <View style={styles.showbtnview}>
      <TouchableOpacity onPress={()=>{this.setState({flip37: !this.state.flip37})}}  >
                   <Image source={showbtn} style={styles.showbtnImage} />
      </TouchableOpacity>
         </View>
      </View>
      {/* Card 38 */}
      <View style={styles.slide}>
         <View style={styles.cardflip}>
            <FlipCard flip={this.state.flip38} style={styles.card} friction={6} perspective={1000} flipHorizontal={true} flipVertical={false} clickable={true} alignHeight={true} onFlipEnd={(isFlipEnd)=>{console.log('isFlipEnd', isFlipEnd)}}>

             {/* Face Side */}
             <View style={styles.face}>
                 <Image source={front_card} style={styles.cardImage}  />
             </View>

             {/* Back Side */}
             <View style={styles.back}>
                 <Image source={back_card38} style={styles.cardImage} />
             </View>
      </FlipCard>
         </View>
         <View style={styles.showbtnview}>
      <TouchableOpacity onPress={()=>{this.setState({flip38: !this.state.flip38})}}  >
                   <Image source={showbtn} style={styles.showbtnImage} />
      </TouchableOpacity>
         </View>
      </View>
      {/* Card 39 */}
      <View style={styles.slide}>
         <View style={styles.cardflip}>
            <FlipCard flip={this.state.flip39} style={styles.card} friction={6} perspective={1000} flipHorizontal={true} flipVertical={false} clickable={true} alignHeight={true} onFlipEnd={(isFlipEnd)=>{console.log('isFlipEnd', isFlipEnd)}}>

             {/* Face Side */}
             <View style={styles.face}>
                 <Image source={front_card} style={styles.cardImage}  />
             </View>

             {/* Back Side */}
             <View style={styles.back}>
                 <Image source={back_card39} style={styles.cardImage} />
             </View>
      </FlipCard>
         </View>
         <View style={styles.showbtnview}>
      <TouchableOpacity onPress={()=>{this.setState({flip39: !this.state.flip39})}}  >
                   <Image source={showbtn} style={styles.showbtnImage} />
      </TouchableOpacity>
         </View>
      </View>
      {/* Card 40 */}
      <View style={styles.slide}>
         <View style={styles.cardflip}>
            <FlipCard flip={this.state.flip40} style={styles.card} friction={6} perspective={1000} flipHorizontal={true} flipVertical={false} clickable={true} alignHeight={true} onFlipEnd={(isFlipEnd)=>{console.log('isFlipEnd', isFlipEnd)}}>

             {/* Face Side */}
             <View style={styles.face}>
                 <Image source={front_card} style={styles.cardImage}  />
             </View>

             {/* Back Side */}
             <View style={styles.back}>
                 <Image source={back_card40} style={styles.cardImage} />
             </View>
      </FlipCard>
         </View>
         <View style={styles.showbtnview}>
      <TouchableOpacity onPress={()=>{this.setState({flip40: !this.state.flip40})}}  >
                   <Image source={showbtn} style={styles.showbtnImage} />
      </TouchableOpacity>
         </View>
      </View>
      {/* Card 41 */}
            <View style={styles.slide}>
               <View style={styles.cardflip}>
                  <FlipCard flip={this.state.flip41} style={styles.card} friction={6} perspective={1000} flipHorizontal={true} flipVertical={false} clickable={true} alignHeight={true} onFlipEnd={(isFlipEnd)=>{console.log('isFlipEnd', isFlipEnd)}}>

                   {/* Face Side */}
                   <View style={styles.face}>
                       <Image source={front_card} style={styles.cardImage}  />
                   </View>

                   {/* Back Side */}
                   <View style={styles.back}>
                       <Image source={back_card41} style={styles.cardImage} />
                   </View>
         </FlipCard>
               </View>
               <View style={styles.showbtnview}>
         <TouchableOpacity onPress={()=>{this.setState({flip56: !this.state.flip41})}}  >
                         <Image source={showbtn} style={styles.showbtnImage} />
         </TouchableOpacity>
               </View>
           </View>



      {/* Card 42 */}
      <View style={styles.slide}>
         <View style={styles.cardflip}>
            <FlipCard flip={this.state.flip42} style={styles.card} friction={6} perspective={1000} flipHorizontal={true} flipVertical={false} clickable={true} alignHeight={true} onFlipEnd={(isFlipEnd)=>{console.log('isFlipEnd', isFlipEnd)}}>

             {/* Face Side */}
             <View style={styles.face}>
                 <Image source={front_card} style={styles.cardImage}  />
             </View>

             {/* Back Side */}
             <View style={styles.back}>
                 <Image source={back_card42} style={styles.cardImage} />
             </View>
      </FlipCard>
         </View>
         <View style={styles.showbtnview}>
      <TouchableOpacity onPress={()=>{this.setState({flip42: !this.state.flip42})}}  >
                   <Image source={showbtn} style={styles.showbtnImage} />
      </TouchableOpacity>
         </View>
      </View>
      {/* Card 43 */}
      <View style={styles.slide}>
         <View style={styles.cardflip}>
            <FlipCard flip={this.state.flip43} style={styles.card} friction={6} perspective={1000} flipHorizontal={true} flipVertical={false} clickable={true} alignHeight={true} onFlipEnd={(isFlipEnd)=>{console.log('isFlipEnd', isFlipEnd)}}>

             {/* Face Side */}
             <View style={styles.face}>
                 <Image source={front_card} style={styles.cardImage}  />
             </View>

             {/* Back Side */}
             <View style={styles.back}>
                 <Image source={back_card43} style={styles.cardImage} />
             </View>
      </FlipCard>
         </View>
         <View style={styles.showbtnview}>
      <TouchableOpacity onPress={()=>{this.setState({flip43: !this.state.flip43})}}  >
                   <Image source={showbtn} style={styles.showbtnImage} />
      </TouchableOpacity>
         </View>
      </View>
      {/* Card 44 */}
      <View style={styles.slide}>
         <View style={styles.cardflip}>
            <FlipCard flip={this.state.flip44} style={styles.card} friction={6} perspective={1000} flipHorizontal={true} flipVertical={false} clickable={true} alignHeight={true} onFlipEnd={(isFlipEnd)=>{console.log('isFlipEnd', isFlipEnd)}}>

             {/* Face Side */}
             <View style={styles.face}>
                 <Image source={front_card} style={styles.cardImage}  />
             </View>

             {/* Back Side */}
             <View style={styles.back}>
                 <Image source={back_card44} style={styles.cardImage} />
             </View>
      </FlipCard>
         </View>
         <View style={styles.showbtnview}>
      <TouchableOpacity onPress={()=>{this.setState({flip44: !this.state.flip44})}}  >
                   <Image source={showbtn} style={styles.showbtnImage} />
      </TouchableOpacity>
         </View>
      </View>
      {/* Card 45 */}
      <View style={styles.slide}>
         <View style={styles.cardflip}>
            <FlipCard flip={this.state.flip45} style={styles.card} friction={6} perspective={1000} flipHorizontal={true} flipVertical={false} clickable={true} alignHeight={true} onFlipEnd={(isFlipEnd)=>{console.log('isFlipEnd', isFlipEnd)}}>

             {/* Face Side */}
             <View style={styles.face}>
                 <Image source={front_card} style={styles.cardImage}  />
             </View>

             {/* Back Side */}
             <View style={styles.back}>
                 <Image source={back_card45} style={styles.cardImage} />
             </View>
      </FlipCard>
         </View>
         <View style={styles.showbtnview}>
      <TouchableOpacity onPress={()=>{this.setState({flip45: !this.state.flip45})}}  >
                   <Image source={showbtn} style={styles.showbtnImage} />
      </TouchableOpacity>
         </View>
      </View>
      {/* Card 46 */}
      <View style={styles.slide}>
         <View style={styles.cardflip}>
            <FlipCard flip={this.state.flip46} style={styles.card} friction={6} perspective={1000} flipHorizontal={true} flipVertical={false} clickable={true} alignHeight={true} onFlipEnd={(isFlipEnd)=>{console.log('isFlipEnd', isFlipEnd)}}>

             {/* Face Side */}
             <View style={styles.face}>
                 <Image source={front_card} style={styles.cardImage}  />
             </View>

             {/* Back Side */}
             <View style={styles.back}>
                 <Image source={back_card46} style={styles.cardImage} />
             </View>
      </FlipCard>
         </View>
         <View style={styles.showbtnview}>
      <TouchableOpacity onPress={()=>{this.setState({flip46: !this.state.flip46})}}  >
                   <Image source={showbtn} style={styles.showbtnImage} />
      </TouchableOpacity>
         </View>
      </View>

      {/* Card 47 */}
      <View style={styles.slide}>
         <View style={styles.cardflip}>
            <FlipCard flip={this.state.flip47} style={styles.card} friction={6} perspective={1000} flipHorizontal={true} flipVertical={false} clickable={true} alignHeight={true} onFlipEnd={(isFlipEnd)=>{console.log('isFlipEnd', isFlipEnd)}}>

             {/* Face Side */}
             <View style={styles.face}>
                 <Image source={front_card} style={styles.cardImage}  />
             </View>

             {/* Back Side */}
             <View style={styles.back}>
                 <Image source={back_card47} style={styles.cardImage} />
             </View>
      </FlipCard>
         </View>
         <View style={styles.showbtnview}>
      <TouchableOpacity onPress={()=>{this.setState({flip47: !this.state.flip47})}}  >
                   <Image source={showbtn} style={styles.showbtnImage} />
      </TouchableOpacity>
         </View>
      </View>
      {/* Card 48 */}
      <View style={styles.slide}>
         <View style={styles.cardflip}>
            <FlipCard flip={this.state.flip48} style={styles.card} friction={6} perspective={1000} flipHorizontal={true} flipVertical={false} clickable={true} alignHeight={true} onFlipEnd={(isFlipEnd)=>{console.log('isFlipEnd', isFlipEnd)}}>

             {/* Face Side */}
             <View style={styles.face}>
                 <Image source={front_card} style={styles.cardImage}  />
             </View>

             {/* Back Side */}
             <View style={styles.back}>
                 <Image source={back_card48} style={styles.cardImage} />
             </View>
      </FlipCard>
         </View>
         <View style={styles.showbtnview}>
      <TouchableOpacity onPress={()=>{this.setState({flip48: !this.state.flip48})}}  >
                   <Image source={showbtn} style={styles.showbtnImage} />
      </TouchableOpacity>
         </View>
      </View>
      {/* Card 49 */}
      <View style={styles.slide}>
         <View style={styles.cardflip}>
            <FlipCard flip={this.state.flip49} style={styles.card} friction={6} perspective={1000} flipHorizontal={true} flipVertical={false} clickable={true} alignHeight={true} onFlipEnd={(isFlipEnd)=>{console.log('isFlipEnd', isFlipEnd)}}>

             {/* Face Side */}
             <View style={styles.face}>
                 <Image source={front_card} style={styles.cardImage}  />
             </View>

             {/* Back Side */}
             <View style={styles.back}>
                 <Image source={back_card49} style={styles.cardImage} />
             </View>
      </FlipCard>
         </View>
         <View style={styles.showbtnview}>
      <TouchableOpacity onPress={()=>{this.setState({flip49: !this.state.flip49})}}  >
                   <Image source={showbtn} style={styles.showbtnImage} />
      </TouchableOpacity>
         </View>
      </View>
      {/* Card 50 */}
      <View style={styles.slide}>
         <View style={styles.cardflip}>
            <FlipCard flip={this.state.flip50} style={styles.card} friction={6} perspective={1000} flipHorizontal={true} flipVertical={false} clickable={true} alignHeight={true} onFlipEnd={(isFlipEnd)=>{console.log('isFlipEnd', isFlipEnd)}}>

             {/* Face Side */}
             <View style={styles.face}>
                 <Image source={front_card} style={styles.cardImage}  />
             </View>

             {/* Back Side */}
             <View style={styles.back}>
                 <Image source={back_card50} style={styles.cardImage} />
             </View>
      </FlipCard>
         </View>
         <View style={styles.showbtnview}>
      <TouchableOpacity onPress={()=>{this.setState({flip50: !this.state.flip50})}}  >
                   <Image source={showbtn} style={styles.showbtnImage} />
      </TouchableOpacity>
         </View>
      </View>

      {/* Card 51 */}
      <View style={styles.slide}>
         <View style={styles.cardflip}>
            <FlipCard flip={this.state.flip51} style={styles.card} friction={6} perspective={1000} flipHorizontal={true} flipVertical={false} clickable={true} alignHeight={true} onFlipEnd={(isFlipEnd)=>{console.log('isFlipEnd', isFlipEnd)}}>

             {/* Face Side */}
             <View style={styles.face}>
                 <Image source={front_card} style={styles.cardImage}  />
             </View>

             {/* Back Side */}
             <View style={styles.back}>
                 <Image source={back_card51} style={styles.cardImage} />
             </View>
      </FlipCard>
         </View>
         <View style={styles.showbtnview}>
      <TouchableOpacity onPress={()=>{this.setState({flip51: !this.state.flip51})}}  >
                   <Image source={showbtn} style={styles.showbtnImage} />
      </TouchableOpacity>
         </View>
      </View>
      {/* Card 52 */}
      <View style={styles.slide}>
         <View style={styles.cardflip}>
            <FlipCard flip={this.state.flip52} style={styles.card} friction={6} perspective={1000} flipHorizontal={true} flipVertical={false} clickable={true} alignHeight={true} onFlipEnd={(isFlipEnd)=>{console.log('isFlipEnd', isFlipEnd)}}>

             {/* Face Side */}
             <View style={styles.face}>
                 <Image source={front_card} style={styles.cardImage}  />
             </View>

             {/* Back Side */}
             <View style={styles.back}>
                 <Image source={back_card52} style={styles.cardImage} />
             </View>
      </FlipCard>
         </View>
         <View style={styles.showbtnview}>
      <TouchableOpacity onPress={()=>{this.setState({flip52: !this.state.flip52})}}  >
                   <Image source={showbtn} style={styles.showbtnImage} />
      </TouchableOpacity>
         </View>
      </View>
      {/* Card 53 */}
      <View style={styles.slide}>
         <View style={styles.cardflip}>
            <FlipCard flip={this.state.flip53} style={styles.card} friction={6} perspective={1000} flipHorizontal={true} flipVertical={false} clickable={true} alignHeight={true} onFlipEnd={(isFlipEnd)=>{console.log('isFlipEnd', isFlipEnd)}}>

             {/* Face Side */}
             <View style={styles.face}>
                 <Image source={front_card} style={styles.cardImage}  />
             </View>

             {/* Back Side */}
             <View style={styles.back}>
                 <Image source={back_card53} style={styles.cardImage} />
             </View>
      </FlipCard>
         </View>
         <View style={styles.showbtnview}>
      <TouchableOpacity onPress={()=>{this.setState({flip53: !this.state.flip53})}}  >
                   <Image source={showbtn} style={styles.showbtnImage} />
      </TouchableOpacity>
         </View>
      </View>
      {/* Card 54 */}
      <View style={styles.slide}>
         <View style={styles.cardflip}>
            <FlipCard flip={this.state.flip54} style={styles.card} friction={6} perspective={1000} flipHorizontal={true} flipVertical={false} clickable={true} alignHeight={true} onFlipEnd={(isFlipEnd)=>{console.log('isFlipEnd', isFlipEnd)}}>

             {/* Face Side */}
             <View style={styles.face}>
                 <Image source={front_card} style={styles.cardImage}  />
             </View>

             {/* Back Side */}
             <View style={styles.back}>
                 <Image source={back_card54} style={styles.cardImage} />
             </View>
      </FlipCard>
         </View>
         <View style={styles.showbtnview}>
      <TouchableOpacity onPress={()=>{this.setState({flip54: !this.state.flip54})}}  >
                   <Image source={showbtn} style={styles.showbtnImage} />
      </TouchableOpacity>
         </View>
      </View>
      {/* Card 55 */}
      <View style={styles.slide}>
         <View style={styles.cardflip}>
            <FlipCard flip={this.state.flip55} style={styles.card} friction={6} perspective={1000} flipHorizontal={true} flipVertical={false} clickable={true} alignHeight={true} onFlipEnd={(isFlipEnd)=>{console.log('isFlipEnd', isFlipEnd)}}>

             {/* Face Side */}
             <View style={styles.face}>
                 <Image source={front_card} style={styles.cardImage}  />
             </View>

             {/* Back Side */}
             <View style={styles.back}>
                 <Image source={back_card55} style={styles.cardImage} />
             </View>
      </FlipCard>
         </View>
         <View style={styles.showbtnview}>
      <TouchableOpacity onPress={()=>{this.setState({flip55: !this.state.flip55})}}  >
                   <Image source={showbtn} style={styles.showbtnImage} />
      </TouchableOpacity>
         </View>
      </View>

            </Swiper>
        </View>


    </LinearGradient>
      </View>
    </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#3bafca',
  },
  subcontainer: {
    justifyContent: 'center',
    alignItems: 'center',
    /*backgroundColor: '#3bafca',*/
  },

  card:{
    borderWidth: 0,
  },
  slide: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },

  showbtnText:{
      color:'#98bbcd',
      textAlign:'center',
  },
buttonText:{
  color:'#fff',
  justifyContent:'center',
  lineHeight:13,
  fontSize:13,
},
buttonWrapperStyle :{
  backgroundColor: 'transparent',
  flexDirection: 'row',
  position: 'absolute',
  top: '-8%',
  left: 0,
  paddingHorizontal: 5,
  paddingVertical: 5,
  justifyContent: 'space-between',
  alignItems: 'center'
},
swiperbuttonText:{
  color:'#fff',
  fontSize:40,
  fontWeight:'100',
  textShadowColor: 'rgba(0, 0, 0, 0.75)',
  textShadowOffset: {width: -1, height: 1},
  textShadowRadius: 10,

},

showbtnview:{
	flex:.05,
  marginBottom:10,
  height:100,
},
showbtnImage :{
resizeMode:'contain', height: 120, width: 120
},
cardImage :{
 resizeMode:'contain',
 width :AppSizes.screen.width-40,
 height: Platform.OS === 'android' ? AppSizes.screen.height-170 : AppSizes.screen.height-150,
},
cardflip:{
  flex:.85,
}
});
