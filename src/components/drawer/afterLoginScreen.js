import React, { Component } from 'react';
import {StyleSheet,Text,View,Image,TouchableOpacity,Platform,StatusBar} from 'react-native';
import { AppStyles, AppSizes, AppColors } from '../../themes/'
import { NavigationActions } from 'react-navigation'
import LinearGradient from 'react-native-linear-gradient';
import Spacer from '../common/Spacer'

var Constant = require('../../api/ApiRules').Constant;

/* Images */
var logo = require( '../../themes/Images/logo.png')
var continuebtn = require( '../../themes/Images/continuebtn_300.png')
const MyStatusBar = ({backgroundColor, ...props}) => (
  <View style={[styles.statusBar, { backgroundColor }]}>
    <StatusBar translucent backgroundColor={backgroundColor} {...props} />
  </View>
);
export default class afterLoginScreen extends Component {
  constructor (props) {
    super(props)
    this.state = {
    isVisible: false,
  },
  _that = this;
}

show() {
  const resetAction = NavigationActions.navigate({ routeName: 'StartScreen'})
  _that.props.navigation.dispatch(resetAction);
}


  render() {
    return (
      <View style={styles.container}>
      <MyStatusBar barStyle="light-content"  backgroundColor="#3baeca"/>
      <View style={styles.subcontainer}>
      <LinearGradient colors={['#3baeca', '#3baeca', '#3983ab', '#386b98']}>
      <View style={{ flex:.07,flexDirection: 'row', justifyContent: 'center', alignItems: 'center', marginTop:'25%',marginBottom: '5%'}}>

          <View style={{ marginTop:'5%',alignItems: 'center',}}>
              <Image source={logo} style={{resizeMode: 'contain', height: AppSizes.screen.width/2, width: AppSizes.screen.width/2}} />
          </View>
          <View style={{ marginTop:0}}>

          </View>
      </View>

      <View style={{flex:.95, width: AppSizes.screen.width,}}>
        <View style={{ margin:15}}>
        {/*  <Text style={styles.textstyle}>
          Get the Entire Set of Empowered Nurses Clarity Cards for 60% off  </Text>
          <Spacer size={10} />
          <Text style={styles.textstyle}>Empower yourself or share as a gift! </Text>
          <Spacer size={10} />
          <Text style={styles.textstyle}>This beautiful 50-card set is usually $24.99 but to thank you for getting the “online version” I want to give you access to the entire set of actual cards for only $15! </Text>
          <Spacer size={10} />
          <Text style={styles.textstyle}>Are there times when you just want something you can hold on to – like when you hold your favorite book in your lap, instead of downloading it on your tablet? Looking through these cards will make a stressful day, a little better, a little brighter and a lot more fun. </Text>
          <Spacer size={10} />
          <Text style={styles.textstyle}>Give yourself or a special nurse in your life a gift that will make a difference. Click here to order a set of clarity cards today for only $15 and I’ll get it shipped out to you right away.  </Text>
          <Spacer size={10} />
          <Text style={styles.textstyle}>*This limited-time offer of 60% off is not available anywhere else.  </Text>*/}
          <Text style={styles.textstyle}>
          Thanks for downloading the Empowered Nurses Clarity Cards App. I am sure you will love it. Here is how to use the cards... </Text>
          <Spacer size={10} />
          <Text style={styles.textstyle}>  The most common way to use the cards is to simply think or a question or ask a question out loud. Next scroll through the cards and pick a card that calls to you. Click show and read your message. To turn the card back over, click show again. You will receive the guidance you need most. To learn more about how to use the cards, click on the menu bar, then click on How to Use the Cards.
            </Text>
        </View>

        <View style={{ flex:.25,alignItems:'center', justifyContent: 'center'}}>

          <TouchableOpacity activeOpacity={.6} onPress={this.show}>
              <Image source={continuebtn} style={styles.showbtnImage} />
          </TouchableOpacity>
        </View>
      </View>
      </LinearGradient>
      </View>
    </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop:  0,
    backgroundColor: '#3bafca',
  },
  statusBar: {
    height: AppSizes.statusBarHeight,
  },
  subcontainer: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    /*backgroundColor: '#3bafca',*/
  },
  textstyle :{
    fontSize:15,
    fontWeight:'bold',
    color:'#fff'
  },
  showbtnImage :{
  resizeMode:'contain',  width: 160
  },

});
