import React, { Component } from 'react';
import {StyleSheet,Text,View,Image,TouchableOpacity,WebView,Platform, ActivityIndicator,StatusBar} from 'react-native';
import { AppStyles, AppSizes, AppColors } from '../../themes/'
import { NavigationActions } from 'react-navigation'

var Constant = require('../../api/ApiRules').Constant;

/* Images */
var logo = require( '../../themes/Images/logo.png')
var menu_icon = require( '../../themes/Images/menu_icon_144.png')
var back_icon = require( '../../themes/Images/back_120.png')
var home_icon = require( '../../themes/Images/home_icon_48.png')
var aboutcreator = require( '../../themes/Images/aboutcreator.png')

export default class AboutCreatorScreen extends Component {
  constructor (props) {
    super(props)
    this.state = {
    isVisible: false,

  },
  _that = this;
}

  menu_click() {
      _that.props.navigation.openDrawer();
  }

  home_click() {
      _that.props.navigation.navigate('CardScreen');
  }


  render() {
    return (

      <View style={styles.container}>
      <StatusBar hidden={true} />
      <View style={{ flex:.07,flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', marginTop: 20,}}>
          <View style={{ marginTop:0, paddingLeft:10}}>
              <TouchableOpacity onPress={this.menu_click}>
                  <Image source={menu_icon} style={{resizeMode: 'contain', width: 30, height: 30, }} />
              </TouchableOpacity>
          </View>
          <View style={{ marginTop:0}}>
              <Image source={logo} style={{resizeMode: 'contain', height: 200, width: 200}} />
          </View>
          <View style={{ marginTop:0,paddingRight:10}}>
            <TouchableOpacity onPress={this.home_click}>
                <Image source={home_icon} style={{resizeMode: 'contain', width: 40, height: 40, }} />
            </TouchableOpacity>
            </View>
      </View>

      <View style={{flex:.95, marginTop:20}}>
        <Image source={aboutcreator} style={styles.cardImage} />
      </View>
      {/*<View style={{position:'absolute',bottom:10,right:20,alignSelf:'flex-end'}}>
        <TouchableOpacity onPress={this.go_back}>
            <Image source={back_icon} style={{resizeMode: 'contain', width: 60, height: 60, }} />
        </TouchableOpacity>
        </View> */}
    </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop:  0,
    backgroundColor: '#3bafca',
  },
  WebViewStyle:
  {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop:  0,
  },
  cardImage :{
   resizeMode:'contain',
   width :AppSizes.screen.width,
   height: AppSizes.screen.height-80
  },

});
