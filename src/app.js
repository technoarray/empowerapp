import React, {Component} from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { StackNavigator } from 'react-navigation';
import { Platform, StyleSheet, TouchableOpacity, Image,Text, View } from 'react-native';
import SplashScreen from 'react-native-smart-splash-screen'

import { AppStyles, AppSizes, AppColors } from './themes/'

import LoginScreen from './components/drawer/LoginScreen';
import afterLoginScreen from './components/drawer/afterLoginScreen';
import StartScreen from './components/drawer/StartScreen';
import CardScreen from './components/drawer/CardScreen';

//import ShopScreen from './components/drawer/ShopScreen';


const createStackNavigator = currentUser => StackNavigator({
    //New
    StartScreen: {
        screen: StartScreen,
        navigationOptions: {
            header: null
        }
    },
    LoginScreen: {
        screen: LoginScreen,
        navigationOptions: {
            header: null
        }
    },
    afterLoginScreen: {
        screen: afterLoginScreen,
        navigationOptions: {
            header: null
        }
    },

    CardScreen: {
        screen: CardScreen,
    },


}, {
    //initialRouteName: isEmpty(currentUser) ? 'StartScreen' : 'LoginScreen',
    initialRouteName: 'LoginScreen',
    headerMode: 'none',

});

class App extends Component {

    componentWillMount() {
        if (Platform.OS === 'android') {
            SplashScreen.close({
                animationType: SplashScreen.animationType.scale,
                duration: 850,
                delay: 2000
            })
        }
    }
    render() {
        const { currentUser } = this.props;
        const Navigator = createStackNavigator(currentUser);
        return ( < Navigator / > );
    }
}

App.propTypes = {
    currentUser: PropTypes.object.isRequired,
};

const mapStateToProps = store => ({
    currentUser: store.currentUser,
});

export default connect(mapStateToProps)(App);

const styles = StyleSheet.create({
    text: {
        color: '#fff',
        fontSize: 20,
        fontWeight: 'bold'
    },
    imageSizes:{
        height: AppSizes.screen.height/7, width:AppSizes.screen.width/5, resizeMode:'cover'
    },
    imageSizes_header:{
        height: 40, width:40, resizeMode:'cover'
    },
    imageSizes_header_logo:{
        height: 50, width:50, resizeMode:'cover'
    }
});
